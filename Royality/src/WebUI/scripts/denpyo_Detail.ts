﻿import { HinbanTourokuDialog } from "./hinban_touroku.js";
import { Constants } from "./_Constants.js";

$(document).ready(function () {

    function getFormData() {
        ///部門テーブルの値取得
        ///
        /////
        var denpyoInfo = {
            "IsNewDenpyo": isNewDenpyo(),
            "SiharaiSakiCode": $("#SiharaiSakiCode").val(),
            "DenHizuke": $("#DenHizuke").val(),
            "DenNo": $("#DenNo").val(),
            "DenTax": $("#DenTax").val(),
            "Syoukei": $("#Syoukei").val(),
            "Syohizei": $("#Syohizei").val(),
            "Goukei": $("#Goukei").val()
        }

        return denpyoInfo;
    }

    function isNewDenpyo() {
        var isNew = $(".denpyoNumber").attr("data-is-new");

        return isNew;
    }

    function hinbanTorokuDialog(): void {
        ///データ取得バリデーション
        var denNo = $("#DenNo").val();

        ShowHinbanTourokuDlgAjax(denNo);
    }

    function SubmitFrom() {
        var denNo = $("#DenNo").val();
        saveDenpyoAjax();
    }

    //=====================↓↓　Ajax　↓↓　=====================//

    ///ダイアログ表示
    function ShowHinbanTourokuDlgAjax(denNo: any): void {
        var url = $("#hinban_touroku").data("url");
        $("#overlay").find(".overlayMessage").text(Constants.LOADING_DATA);
        $("#overlay").fadeIn(function () {
            $.ajax({
                url: url, // HinbanTourokuDialog/Index
                type: "POST",
                success: function (response) {
                    var hinbanTourokuDlg = $("#hinban_touroku").empty();
                    hinbanTourokuDlg.html(response);
                    HinbanTourokuDialog.initializeDialoge(denNo);
                    ((hinbanTourokuDlg) as any).modal("show");
                },
                complete: function (response) {
                    $("#overlay").fadeOut();
                }
            });
        });
    }

    ///保存伝票表示
    function saveDenpyoAjax() {
        var denpyoInfo = getFormData();
        $("#overlay").find(".overlayMessage").text(Constants.LOADING_DATA);
        $("#overlay").fadeIn(function () {
            $.ajax({
                url: "/DenpyoDetail/Save",
                type: "POST",
                dataType: "json",
                data: { DenpyoInfo: denpyoInfo },
                global: false,
                success: function (response) {
                    $("#DenNo").hide();
                    $("#DenNoLbl").show();
                    $("#DenNo").val(response.result);
                    $("#DenNoLbl").text(response.result);
                },
                complete: function (response) {
                    $("#overlay").fadeOut();
                }
            });
        });
    }

    //=====================↑↑　Ajax　↑↑　=====================//

    //=====================↓↓　Events　↓↓　=====================//
    /* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */
    /* _/ 保存                      _/ */
    $("#btnSave").click(function () {
        SubmitFrom();
    });


    /* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */
    /* _/ 品番。按分設定登録         _/ */
    $("#btnHinbanToroku").click(function (): void {
        hinbanTorokuDialog();
    });

    /* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */
    /* _/ 支払先検索                 _/ */
    $('#btnGetSiharaiSaki').click(function (): void {
        GetSiharaiSakiListDlg(true);
    });

    //$(document).on("focusout", "#DenTax", function () {
    //    ////0でnullだったらアラート警告
    //    if ($(this).val() == 0 || $(this).val() == "") {
    //        var shouldAllowZero = confirm("0の値を税率に設定します。よろしいですか。");

    //        if (shouldAllowZero == false) {
    //            $(this).val("10");
    //        }
    //    }
    //});





});