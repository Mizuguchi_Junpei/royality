﻿import { Constants } from "./_Constants.js";
export var HinbanTourokuDialog = (function () {
    var hinbanSpSheetoptions = {
        colHeaders: ["自社品番", "メーカー品番", "数量", "単価", "金額"],
        colWidths: [100, 90, 50, 45, 65],
        columns: [
            { type: "text", readOnly: true },
            { type: "text" },
            { type: "text" },
            { type: "numeric", decimal: "." },
            { type: "numeric", decimal: "." },
        ],
        minDimensions: [4, 200], //列、行を指定
        //rowResize: false,
        //columnDrag: false,
        columnSorting: false, //並び替え機能
        contextMenu: false,   //
        // Allow column dragging
        //columnDrag: false,
        // Allow column resizing
        //columnResize: true,
        // Allow row resizing
        //rowResize: false,
        // Allow row dragging
        rowDrag: false,
        // Allow table edition
        //editable: true,
        //allowInsertRow: true,
        // Allow new rows
        allowManualInsertRow: false,
        // Allow new columns
        //allowInsertColumn: true,
        // Allow new rows
        allowManualInsertColumn: false,
        // Allow row delete
        allowDeleteRow: false,
        // Allow deleting of all rows
        allowDeletingAllRows: false,
        // Allow column delete
        allowDeleteColumn: false,
        // Allow rename column
        allowRenameColumn: false,
        // Allow comments
        //allowComments: false,
        // Global wrap
        wordWrap: false,
        //onchange: onHinbanSheetCellChanged,
        //onafterchanges: afterAllChanges,
    };

    var uriageResultDataTableOptions = {
        "aaData": "",
        "aoColumns": [
            { "mDataProp": "tenpoCode" },
            { "mDataProp": "tenpoName" },
            { "mDataProp": "anbunAmt" },
            { "mDataProp": "anbunRitsu" },
        ],
        "ordering": false,
        "paging": false,
        //"autoWidth": false,
        "lengthChange": false,
        "searching": false,
        "info": false,
    }

    var bumonResultDataTableOptions = {
        "aaData": "",
        "aoColumns": [
            { "mDataProp": "bummonName" },
            { "mDataProp": "kingaku" },
        ],
        "ordering": false,
        "paging": false,
        //"autoWidth": false,
        "lengthChange": false,
        "searching": false,
        "info": false,
    }

    var JexcelDenUriageResult: any;
    var denpyoNo: string;
    var anbunSett: string;
    var $dtAnbunResultUriage: DataTables.Api;
    var $dtAnbunResultBummon: DataTables.Api;

    /* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */
    /* _/       按分設定子画面の初期設定                  */
    /* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */
    var initializeDialoge = function (denNo: string): void {
        denpyoNo = denNo;

        JexcelDenUriageResult = ($('#spreadSheetHinban') as any).jexcel(hinbanSpSheetoptions);
        $dtAnbunResultBummon = $('#detail_table').DataTable(bumonResultDataTableOptions);
        $dtAnbunResultUriage = $('#tblAnbunResultUriage').DataTable(uriageResultDataTableOptions);
        $("#btnAnbunKeisan").click(onAnbunKeisanClick);
        $("#btnAutoSet").click(onAutoSettingClick);
    }


    //================================↓↓　Events　↓↓================================//

    var onAnbunKeisanClick = function () {
        //ClearMessages();
        //自動按分設定
        autoAnbunKeisanQueryAjax();
    }

    var onAutoSettingClick = function () {
        var siharaiSakiCode = $("#SiharaiSakiCode").val();
        var denStartDate = $("#DenStartDate").val();
        var denEndDate = $("#DenEndDate").val();

        if (isEmptyOrSpaces(siharaiSakiCode)) {
            return alert(Constants.SIHARAISAKI_NOT_RECORD);
        }
        if (isEmptyOrSpaces(denStartDate)) {
            return alert(Constants.SYUKEIHI_NOT_RECORD);
        }
        if (isEmptyOrSpaces(denEndDate)) {
            return alert(Constants.SYUKEIHI_NOT_RECORD);
        }

        onAutoSettingAjax();
    }

    //================================↑↑　Events　↑↑================================//

    //================================↓↓　Ajax request ↓↓================================//

    var autoAnbunKeisanQueryAjax = function () {
        $("#overlay").find(".overlayMessage").text(Constants.ANBUN_KEISAN_CALCULATION);
        $("#overlay").fadeIn(function () {
            $.ajax({
                url: "/HinbanTourokuDialog/AutoAnbunKeisan",
                type: "POST",
                dataType: "json",
                data: {
                    AnbunSetting: getAnbunSetting(),
                    UriageInfoList: getTargetHinban(),
                },
                success: function (response) {
                    setAutoAnbunResultUriage(response.result.anbunResultListUriage);
                    setAutoAnbunResultBummon(response.result.anbunResultListBummon);
                },
                complete: function () {
                    $("#overlay").fadeOut();
                }
            });
        });
    }

    var onAutoSettingAjax = function () {
        $("#overlay").find(".overlayMessage").text(Constants.LOADING_DATA)
        $("#overlay").fadeIn(function () {
            $.ajax({
                url: "/HinbanTourokuDialog/GetDenpyoUriage",
                type: "POST",
                dataType: "json",
                //dataSrc: "result",
                data: {
                    siharaiSakiCode: function () { return ((document.getElementById("SiharaiSakiCode")) as HTMLInputElement).value },
                    denStartDate: function () { return ((document.getElementById("DenStartDate")) as HTMLInputElement).value },
                    denEndDate: function () { return ((document.getElementById("DenEndDate")) as HTMLInputElement).value },
                },
                success: function (response) {
                    if (response.result) {
                        var table = convertToJexcelTableImp(response.result);
                        JexcelDenUriageResult.setData(table);
                    }
                },
                complete: function (response) {
                    $("#overlay").fadeOut();
                }
            });
        });
    }

    //================================↑↑　Ajax request ↑↑================================//

    var convertToJexcelTableImp = function (hinbanItems: any) {
        var table = [];
        var hinbanCount = hinbanItems.length;
        for (var i = 0; i < hinbanCount; i++) {
            var hinbanRow = hinbanItems[i];
            var row = [];

            row.push(hinbanRow["jisyaHinban"]);
            row.push(hinbanRow["makerHinban"]);
            row.push(hinbanRow["suryo"]);
            row.push(hinbanRow["tanka"]);
            row.push(hinbanRow["goukei"]);

            table.push(row);
        }
        return table;
    }

    ///スプレッドシートデータ取得
    var getTargetHinban = function (): any {
        var spereadSheetData = JexcelDenUriageResult.getData(false);
        var hinbanDataList: any = [];

        spereadSheetData.forEach(function (row: any) {
            if (!isEmptyOrSpaces(row[1])) {
                var hinbandata = {
                    "RoHinban": row[0],
                    "MakerHinban": row[1],
                    "Unit": row[2],
                    "UnitPrice": row[3],
                    "Amount": row[4]
                };
                hinbanDataList.push(hinbandata);
            }
        });
        return hinbanDataList;
    }

    var getAnbunSetting = function (): any {
        var $denStartDate = $('input[id="DenStartDate"]');
        var $denEndDate = $('input[id="DenEndDate"]');

        var anbunSettingInfo = {
            "DenNo": denpyoNo,
            "AnbunMethod": getAnbunMode(),
            //"AnbunSett": anbunSett,
            "StartDate": $denStartDate.val(),
            "EndDate": $denEndDate.val()
        };
        return anbunSettingInfo;
    }

    var getAnbunMode = function (): number {
        var tabID = $(".hinbanTourokuDlgModel .nav-pills .active").attr("id");
        var anbunMode;
        switch (tabID) {
            case "tabAutoAnbun":
                anbunMode = 1;
                break;
            case "tabManualAnbun":
                anbunMode = 2;
                break;
        }
        return anbunMode;
    }

    var setAutoAnbunResultUriage = function (autoAnbunResult: any): void {
        $dtAnbunResultUriage.clear().draw();
        $dtAnbunResultUriage.rows.add(autoAnbunResult);
        $dtAnbunResultUriage.columns.adjust().draw();
    }

    var setAutoAnbunResultBummon = function (autoAnbunResult: any): void {

        //for (var i: number = 1; autoAnbunResult.Count(); i++) {
        //    $("#detail_table").append('<tr><th class= "tableGrid" scope = "row" >' + (++i) +
        //        '< /th>< td >' + (autoAnbunResult.BummonName) + '</td ><td>' + (autoAnbunResult.Kingaku) + '</td></tr > ')
        //}

        $dtAnbunResultBummon.clear().draw();
        $dtAnbunResultBummon.rows.add(autoAnbunResult);
        $dtAnbunResultBummon.columns.adjust().draw();
    }




    return {
        initializeDialoge: initializeDialoge,
    };
})();
