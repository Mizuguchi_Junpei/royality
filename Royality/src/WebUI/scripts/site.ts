﻿/* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */
/* _/ 支払先検索で支払先選択     _/ */
function siharaisaki_select(siharaisaki_code: string, siharaisaki_name: string) {
    // 選択した仕入先を、仕入先コード入力欄と仕入先名欄にセット
    (document.getElementById("SiharaiSakiCode") as HTMLInputElement).value = siharaisaki_code;
    document.getElementById("SiharaiSakiNameLbl").innerHTML = siharaisaki_name;
    
    // モーダルを閉じる
    ($('#siharaisaki_search') as any).modal("hide");
}

/* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */
/* _/ 取引先名自動表示                   _/ */
function siharaisaki_name_set(thisElement: HTMLElement) {
    if (((thisElement)as HTMLInputElement).value.length == 6) {
        if (document.getElementById("sname_" + ((thisElement) as HTMLInputElement).value)) {
            document.getElementById("SiharaiSakiNameLbl").innerHTML = document.getElementById("sname_" + ((thisElement) as HTMLInputElement).value).innerHTML;
        } else {
            document.getElementById("SiharaiSakiNameLbl").innerHTML = "";
        }
    } else {
        document.getElementById("SiharaiSakiNameLbl").innerHTML = "";
    }
}

function GetSiharaiSakiListDlg(isShowDialog: boolean) {
    var url = $('#siharaisaki_search').data('url');
    $('#overlay').find('.overlayMessage').text("処理中...");
    $('#overlay').fadeIn(function () {
        $.ajax({
            url: url,　/// DenpyoList/SiharaiSakiDlg
            success: function (response) {
                $('#siharaisaki_search').html(response);
                if (isShowDialog == true) {
                    ($('#siharaisaki_search') as any).modal("show");
                }
            },
            complete: function () {
                $('#overlay').fadeOut();
            }
        });
    });
}