$(document).ready(function () {


    function hinbanTorokuDialog(): void {
        ///データ取得バリデーション

        ShowHinbanTourokuDlgAjax();
    }

    //=====================↓↓　Ajax　↓↓　=====================//

    function ShowHinbanTourokuDlgAjax(): void {
        var url = $("#hinban_touroku").data("url");
        $("#overlay").find(".overlayMessage").text("処理中...");
        $("#overlay").fadeIn(function () {
            $.ajax({
                url: url, // HinbanTourokuDialog/Index
                type: "POST",
                success: function (response) {
                    var hinbanTourokuDlg = $("#hinban_touroku").empty();
                    hinbanTourokuDlg.html(response);
                    HinbanTourokuDialog.initializeDialoge();
                    ((hinbanTourokuDlg) as any).modal("show");
                },
                complete: function (response) {
                    $("#overlay").fadeOut();
                }
            });
        });
    }

    //=====================↑↑　Ajax　↑↑　=====================//

    //=====================↓↓　Events　↓↓　=====================//

    /* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */
    /* _/ 品番。按分設定登録         _/ */
    $("#btnHinbanToroku").click(function (): void {
        hinbanTorokuDialog();
    });

    /* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */
    /* _/ 支払先検索                 _/ */
    $('#btnGetSiharaiSaki').click(function ():void {
        GetSiharaiSakiListDlg(true);
    });

    //$(document).on("focusout", "#DenTax", function () {
    //    ////0でnullだったらアラート警告
    //    if ($(this).val() == 0 || $(this).val() == "") {
    //        var shouldAllowZero = confirm("0の値を税率に設定します。よろしいですか。");

    //        if (shouldAllowZero == false) {
    //            $(this).val("10");
    //        }
    //    }
    //});

});