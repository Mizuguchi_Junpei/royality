$(document).ready(function () {

    /* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */
    /* + 日付設定のカレンダー表示                            + */
    //$('.datepicker').datepicker({
    //  language: 'ja',
    //  format: "yyyy-mm-dd",
    //  weekStart: "1",
    //  todayHighlight: true,
    //  clearBtn: true,
    //  autoclose: true,
    //  Orientation: "top auto"
    //});

    $(function () {
        $("body").delegate(".datepicker", "focusin", function () {
            ($(this) as any).datepicker({
                language: 'ja',
                format: "yyyy-mm-dd",
                weekStart: "1",
                todayHighlight: true,
                clearBtn: true,
                autoclose: true,
                Orientation: "top auto"
            });
        });
    });

/* + 日付設定のカレンダー表示                            + */
/* +++++++++++++++++++++++++++++++++++++++++++++++++++++++ */

});

function isEmptyOrSpaces(str: any): boolean {
    return str === null || str.match(/^ *$/) !== null;
}