$(document).ready(function () {
    $(function () {
        $("body").delegate(".datepicker", "focusin", function () {
            $(this).datepicker({
                language: 'ja',
                format: "yyyy-mm-dd",
                weekStart: "1",
                todayHighlight: true,
                clearBtn: true,
                autoclose: true,
                Orientation: "top auto"
            });
        });
    });
});
function isEmptyOrSpaces(str) {
    return str === null || str.match(/^ *$/) !== null;
}
//# sourceMappingURL=Royality_kaikei.js.map