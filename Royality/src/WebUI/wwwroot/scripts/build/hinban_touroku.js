import { Constants } from "./_Constants.js";
export var HinbanTourokuDialog = (function () {
    var hinbanSpSheetoptions = {
        colHeaders: ["自社品番", "メーカー品番", "数量", "単価", "金額"],
        colWidths: [100, 90, 50, 45, 65],
        columns: [
            { type: "text", readOnly: true },
            { type: "text" },
            { type: "text" },
            { type: "numeric", decimal: "." },
            { type: "numeric", decimal: "." },
        ],
        minDimensions: [4, 200],
        columnSorting: false,
        contextMenu: false,
        rowDrag: false,
        allowManualInsertRow: false,
        allowManualInsertColumn: false,
        allowDeleteRow: false,
        allowDeletingAllRows: false,
        allowDeleteColumn: false,
        allowRenameColumn: false,
        wordWrap: false,
    };
    var uriageResultDataTableOptions = {
        "aaData": "",
        "aoColumns": [
            { "mDataProp": "tenpoCode" },
            { "mDataProp": "tenpoName" },
            { "mDataProp": "anbunAmt" },
            { "mDataProp": "anbunRitsu" },
        ],
        "ordering": false,
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "info": false,
    };
    var bumonResultDataTableOptions = {
        "aaData": "",
        "aoColumns": [
            { "mDataProp": "bummonName" },
            { "mDataProp": "kingaku" },
        ],
        "ordering": false,
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "info": false,
    };
    var JexcelDenUriageResult;
    var denpyoNo;
    var anbunSett;
    var $dtAnbunResultUriage;
    var $dtAnbunResultBummon;
    var initializeDialoge = function (denNo) {
        denpyoNo = denNo;
        JexcelDenUriageResult = $('#spreadSheetHinban').jexcel(hinbanSpSheetoptions);
        $dtAnbunResultBummon = $('#detail_table').DataTable(bumonResultDataTableOptions);
        $dtAnbunResultUriage = $('#tblAnbunResultUriage').DataTable(uriageResultDataTableOptions);
        $("#btnAnbunKeisan").click(onAnbunKeisanClick);
        $("#btnAutoSet").click(onAutoSettingClick);
    };
    var onAnbunKeisanClick = function () {
        autoAnbunKeisanQueryAjax();
    };
    var onAutoSettingClick = function () {
        var siharaiSakiCode = $("#SiharaiSakiCode").val();
        var denStartDate = $("#DenStartDate").val();
        var denEndDate = $("#DenEndDate").val();
        if (isEmptyOrSpaces(siharaiSakiCode)) {
            return alert(Constants.SIHARAISAKI_NOT_RECORD);
        }
        if (isEmptyOrSpaces(denStartDate)) {
            return alert(Constants.SYUKEIHI_NOT_RECORD);
        }
        if (isEmptyOrSpaces(denEndDate)) {
            return alert(Constants.SYUKEIHI_NOT_RECORD);
        }
        onAutoSettingAjax();
    };
    var autoAnbunKeisanQueryAjax = function () {
        $("#overlay").find(".overlayMessage").text(Constants.ANBUN_KEISAN_CALCULATION);
        $("#overlay").fadeIn(function () {
            $.ajax({
                url: "/HinbanTourokuDialog/AutoAnbunKeisan",
                type: "POST",
                dataType: "json",
                data: {
                    AnbunSetting: getAnbunSetting(),
                    UriageInfoList: getTargetHinban(),
                },
                success: function (response) {
                    setAutoAnbunResultUriage(response.result.anbunResultListUriage);
                    setAutoAnbunResultBummon(response.result.anbunResultListBummon);
                },
                complete: function () {
                    $("#overlay").fadeOut();
                }
            });
        });
    };
    var onAutoSettingAjax = function () {
        $("#overlay").find(".overlayMessage").text(Constants.LOADING_DATA);
        $("#overlay").fadeIn(function () {
            $.ajax({
                url: "/HinbanTourokuDialog/GetDenpyoUriage",
                type: "POST",
                dataType: "json",
                data: {
                    siharaiSakiCode: function () { return (document.getElementById("SiharaiSakiCode")).value; },
                    denStartDate: function () { return (document.getElementById("DenStartDate")).value; },
                    denEndDate: function () { return (document.getElementById("DenEndDate")).value; },
                },
                success: function (response) {
                    if (response.result) {
                        var table = convertToJexcelTableImp(response.result);
                        JexcelDenUriageResult.setData(table);
                    }
                },
                complete: function (response) {
                    $("#overlay").fadeOut();
                }
            });
        });
    };
    var convertToJexcelTableImp = function (hinbanItems) {
        var table = [];
        var hinbanCount = hinbanItems.length;
        for (var i = 0; i < hinbanCount; i++) {
            var hinbanRow = hinbanItems[i];
            var row = [];
            row.push(hinbanRow["jisyaHinban"]);
            row.push(hinbanRow["makerHinban"]);
            row.push(hinbanRow["suryo"]);
            row.push(hinbanRow["tanka"]);
            row.push(hinbanRow["goukei"]);
            table.push(row);
        }
        return table;
    };
    var getTargetHinban = function () {
        var spereadSheetData = JexcelDenUriageResult.getData(false);
        var hinbanDataList = [];
        spereadSheetData.forEach(function (row) {
            if (!isEmptyOrSpaces(row[1])) {
                var hinbandata = {
                    "RoHinban": row[0],
                    "MakerHinban": row[1],
                    "Unit": row[2],
                    "UnitPrice": row[3],
                    "Amount": row[4]
                };
                hinbanDataList.push(hinbandata);
            }
        });
        return hinbanDataList;
    };
    var getAnbunSetting = function () {
        var $denStartDate = $('input[id="DenStartDate"]');
        var $denEndDate = $('input[id="DenEndDate"]');
        var anbunSettingInfo = {
            "DenNo": denpyoNo,
            "AnbunMethod": getAnbunMode(),
            "StartDate": $denStartDate.val(),
            "EndDate": $denEndDate.val()
        };
        return anbunSettingInfo;
    };
    var getAnbunMode = function () {
        var tabID = $(".hinbanTourokuDlgModel .nav-pills .active").attr("id");
        var anbunMode;
        switch (tabID) {
            case "tabAutoAnbun":
                anbunMode = 1;
                break;
            case "tabManualAnbun":
                anbunMode = 2;
                break;
        }
        return anbunMode;
    };
    var setAutoAnbunResultUriage = function (autoAnbunResult) {
        $dtAnbunResultUriage.clear().draw();
        $dtAnbunResultUriage.rows.add(autoAnbunResult);
        $dtAnbunResultUriage.columns.adjust().draw();
    };
    var setAutoAnbunResultBummon = function (autoAnbunResult) {
        $dtAnbunResultBummon.clear().draw();
        $dtAnbunResultBummon.rows.add(autoAnbunResult);
        $dtAnbunResultBummon.columns.adjust().draw();
    };
    return {
        initializeDialoge: initializeDialoge,
    };
})();
//# sourceMappingURL=hinban_touroku.js.map