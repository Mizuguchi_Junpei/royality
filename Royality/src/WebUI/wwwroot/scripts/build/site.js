function siharaisaki_select(siharaisaki_code, siharaisaki_name) {
    document.getElementById("SiharaiSakiCode").value = siharaisaki_code;
    document.getElementById("SiharaiSakiNameLbl").innerHTML = siharaisaki_name;
    $('#siharaisaki_search').modal("hide");
}
function siharaisaki_name_set(thisElement) {
    if ((thisElement).value.length == 6) {
        if (document.getElementById("sname_" + (thisElement).value)) {
            document.getElementById("SiharaiSakiNameLbl").innerHTML = document.getElementById("sname_" + (thisElement).value).innerHTML;
        }
        else {
            document.getElementById("SiharaiSakiNameLbl").innerHTML = "";
        }
    }
    else {
        document.getElementById("SiharaiSakiNameLbl").innerHTML = "";
    }
}
function GetSiharaiSakiListDlg(isShowDialog) {
    var url = $('#siharaisaki_search').data('url');
    $('#overlay').find('.overlayMessage').text("処理中...");
    $('#overlay').fadeIn(function () {
        $.ajax({
            url: url,
            success: function (response) {
                $('#siharaisaki_search').html(response);
                if (isShowDialog == true) {
                    $('#siharaisaki_search').modal("show");
                }
            },
            complete: function () {
                $('#overlay').fadeOut();
            }
        });
    });
}
//# sourceMappingURL=site.js.map