import { HinbanTourokuDialog } from "./hinban_touroku.js";
import { Constants } from "./_Constants.js";
$(document).ready(function () {
    function getFormData() {
        var denpyoInfo = {
            "IsNewDenpyo": isNewDenpyo(),
            "SiharaiSakiCode": $("#SiharaiSakiCode").val(),
            "DenHizuke": $("#DenHizuke").val(),
            "DenNo": $("#DenNo").val(),
            "DenTax": $("#DenTax").val(),
            "Syoukei": $("#Syoukei").val(),
            "Syohizei": $("#Syohizei").val(),
            "Goukei": $("#Goukei").val()
        };
        return denpyoInfo;
    }
    function isNewDenpyo() {
        var isNew = $(".denpyoNumber").attr("data-is-new");
        return isNew;
    }
    function hinbanTorokuDialog() {
        var denNo = $("#DenNo").val();
        ShowHinbanTourokuDlgAjax(denNo);
    }
    function SubmitFrom() {
        var denNo = $("#DenNo").val();
        saveDenpyoAjax();
    }
    function ShowHinbanTourokuDlgAjax(denNo) {
        var url = $("#hinban_touroku").data("url");
        $("#overlay").find(".overlayMessage").text(Constants.LOADING_DATA);
        $("#overlay").fadeIn(function () {
            $.ajax({
                url: url,
                type: "POST",
                success: function (response) {
                    var hinbanTourokuDlg = $("#hinban_touroku").empty();
                    hinbanTourokuDlg.html(response);
                    HinbanTourokuDialog.initializeDialoge(denNo);
                    (hinbanTourokuDlg).modal("show");
                },
                complete: function (response) {
                    $("#overlay").fadeOut();
                }
            });
        });
    }
    function saveDenpyoAjax() {
        var denpyoInfo = getFormData();
        $("#overlay").find(".overlayMessage").text(Constants.LOADING_DATA);
        $("#overlay").fadeIn(function () {
            $.ajax({
                url: "/DenpyoDetail/Save",
                type: "POST",
                dataType: "json",
                data: { DenpyoInfo: denpyoInfo },
                global: false,
                success: function (response) {
                    $("#DenNo").hide();
                    $("#DenNoLbl").show();
                    $("#DenNo").val(response.result);
                    $("#DenNoLbl").text(response.result);
                },
                complete: function (response) {
                    $("#overlay").fadeOut();
                }
            });
        });
    }
    $("#btnSave").click(function () {
        SubmitFrom();
    });
    $("#btnHinbanToroku").click(function () {
        hinbanTorokuDialog();
    });
    $('#btnGetSiharaiSaki').click(function () {
        GetSiharaiSakiListDlg(true);
    });
});
//# sourceMappingURL=denpyo_Detail.js.map