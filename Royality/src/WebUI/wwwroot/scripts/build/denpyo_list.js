$(document).ready(function () {
    var denListTableOption = {
        "language": {
            "decimal": "",
            "emptyTable": "現在データはありません",
            "info": '合計_TOTAL_ 件のレコードの _START_ から _END_ 件までの表示',
            "infoEmpty": "件数は０件です",
            "infoFiltered": "(filtered from _MAX_ total entries)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "_MENU_ 表示伝票件数",
            "loadingRecords": "Loading...",
            "processing": "Processing...",
            "search": "検索",
            "zeroRecords": "レコードは０件です",
            "paginate": {
                "first": "最初",
                "last": "最後",
                "next": "次へ",
                "previous": "前へ"
            },
            "aria": {
                "sortAscending": ": 列の昇順ソートを有効にする",
                "sortDescending": ": アクティブにして列を降順に並べ替える"
            }
        },
        "processing": true,
        "ordering": false,
        "autoWidth": false,
        "responsive": true,
        "ajax": {
            "url": "/DenpyoList/GetDenpyoList",
            "type": "POST",
            "datatype": "json",
            "dataSrc": "result",
            "data": {
                DenStartDate: function () { return document.getElementById('DenStartDate').value; },
                DenEndDate: function () { return document.getElementById('DenEndDate').value; },
                StatusID: function () { return document.getElementById('StatusID').value; },
                SiharaiSakiCode: function () { return document.getElementById('SiharaiSakiCode').value; }
            }
        },
        "columns": [
            { "data": "denNo" },
            { "data": "denNo" },
            { "data": "denHiduke" },
            { "data": "siharaiSaki" },
            { "data": "siharaiSakiMei" },
            { "data": "syokei" },
            { "data": "syohizei" },
            { "data": "goukei" },
            { "data": "kosinsya" },
            { "data": "koshinHi" },
            { "data": "status" }
        ],
        "columnDefs": [
            {
                'render': function (data) {
                    return ' <span class="denListCheckBox"> <input type="checkbox" value= "' + data + '" class="checkbox tblRowCheckBox "/> </span>';
                },
                "targets": 0,
            },
            {
                'render': function (data) {
                    return data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                },
                "targets": 5,
                "className": "text-right",
            },
            {
                'render': function (data) {
                    return data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                },
                "targets": 6,
                "className": "text-right",
            },
            {
                'render': function (data) {
                    return data.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
                },
                "targets": 7,
                "className": "text-right",
            },
        ]
    };
    var $denListTable = $("#sinsei_table").DataTable(denListTableOption);
    var clearSearch = function () {
        document.getElementById("DenStartDate").value = "";
        document.getElementById("DenEndDate").value = "";
        document.getElementById("StatusID").selectedIndex = 0;
        document.getElementById("SiharaiSakiCode").value = "";
        document.getElementById("SiharaiSakiNameLbl").innerHTML = "";
    };
    $("#btnSearch").click(function () {
        $denListTable.ajax.reload(null, false);
    });
    $("#btnGetSiharaiSaki").click(function () {
        GetSiharaiSakiListDlg(true);
    });
    $("#btnSearchClear").click(function () {
        clearSearch();
    });
    $('#chkCheckAll').click(function () {
        if ((this).checked == true) {
            $("#sinsei_table > tbody > tr > td > .denListCheckBox > input[type=checkbox]").prop('checked', true);
        }
        else {
            $("#sinsei_table > tbody > tr > td > .denListCheckBox > input[type=checkbox]").prop('checked', false);
        }
    });
    $('#SiharaiSakiCode').change(function () {
        siharaisaki_name_set(this);
    });
});
//# sourceMappingURL=denpyo_list.js.map