export class Constants {
}
Constants.LOADING_DATA = "処理中...";
Constants.ANBUN_KEISAN_CALCULATION = "按分計算中...";
Constants.SIHARAISAKI_NOT_RECORD = "支払先コードが入力されていません。";
Constants.SYUKEIHI_NOT_RECORD = "集計期間が入力されていません。";
Constants.SHOULD_ZERO_ALLOWED = "0の値を税率に設定します。よろしいですか。";
//# sourceMappingURL=_Constants.js.map