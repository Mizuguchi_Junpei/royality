import { Constants } from "./_Constants";
export var HinbanTourokuDialog = (function () {
    var hinbanSpSheetoptions = {
        colHeaders: ["自社品番", "メーカー品番", "単価", "数量", "金額"],
        colWidths: [100, 90, 50, 45, 65],
        columns: [
            { type: "text" },
            { type: "text" },
            { type: "numeric", decimal: "." },
            { type: "text" },
            { type: "numeric", decimal: "." },
        ],
        minDimensions: [4, 200], //列、行を指定
        //rowResize: false,
        //columnDrag: false,
        columnSorting: false, //並び替え機能
        contextMenu: false,   //
        // Allow column dragging
        //columnDrag: false,
        // Allow column resizing
        //columnResize: true,
        // Allow row resizing
        //rowResize: false,
        // Allow row dragging
        rowDrag: false,
        // Allow table edition
        //editable: true,
        //allowInsertRow: true,
        // Allow new rows
        allowManualInsertRow: false,
        // Allow new columns
        //allowInsertColumn: true,
        // Allow new rows
        allowManualInsertColumn: false,
        // Allow row delete
        allowDeleteRow: false,
        // Allow deleting of all rows
        allowDeletingAllRows: false,
        // Allow column delete
        allowDeleteColumn: false,
        // Allow rename column
        allowRenameColumn: false,
        // Allow comments
        //allowComments: false,
        // Global wrap
        wordWrap: false,
        //onchange: onHinbanSheetCellChanged,
        //onafterchanges: afterAllChanges,
    };


    var JexcelDenUriageResult: any;

    /* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */
    /* _/       按分設定子画面の初期設定                  */
    /* _/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/ */
    var initializeDialoge = function (): void {
        var reasonCode = "";
        var denpyoNo = "";

        var JexcelDenUriageResult = ($('#spreadSheetHinban') as any).jexcel(hinbanSpSheetoptions);
        $("#btnAutoSet").click(onAutoSettingClick);
    }


    //================================↑↑　Events　↑↑================================//
    var onAutoSettingClick = function () {

        var siharaiSakiCode = $("#SiharaiSakiCode").val();
        var denStartDate = $("#DenStartDate").val();
        var denEndDate = $("#DenEndtDate").val();


        if (isEmptyOrSpaces(siharaiSakiCode)) {
            return alert(Constants.SIHARAISAKI_NOT_RECORD);
        }
        if (isEmptyOrSpaces(denStartDate)) {
            return alert(Constants.SYUKEIHI_NOT_RECORD);
        }
        if (isEmptyOrSpaces(denEndDate)) {
            return alert(Constants.SYUKEIHI_NOT_RECORD);
        }

        onAutoSettingAjax();
    }

    //================================↑↑　Events　↑↑================================//

    //================================↓↓　Ajax request ↓↓================================//


    var onAutoSettingAjax = function (): void {
        $("#overlay").find(".overlayMessage").text(Constants.LOADING_DATA)
        $("#overlay").fadeIn(function () {
            $.ajax({
                url: "/HinbanTourokuDialog/GetDenpyoUriage",
                type: "POST",
                dataType: "json",
                //dataSrc: "result",
                data: {
                    siharaiSakiCode: function () { return ((document.getElementById("SiharaiSakiCode")) as HTMLInputElement).value },
                    denStartDate: function () { return ((document.getElementById("DenStartDate")) as HTMLInputElement).value },
                    denEndDate: function () { return ((document.getElementById("DenEtDate")) as HTMLInputElement).value },
                },
                success: function (response) {
                    if (response.result) {
                        var table = convertToJexcelTableImp(response.result);
                        JexcelDenUriageResult.setData(table);
                    }
                },
                complete: function (response) {
                    $("#overlay").fadeOut();
                }

            });
        });
    }

    //================================↑↑　Ajax request ↑↑================================//

    var convertToJexcelTableImp = function (hinbanItems: any) {
        var table = [];
        var hinbanCount = hinbanItems.length;
        for (var i = 0; i < hinbanCount; i++) {
            var hinbanRow = hinbanItems[i];
            var row = [];

            row.push(hinbanRow["Jisyahinban"]);
            row.push(hinbanRow["meakerHinban"]);
            row.push(hinbanRow["tanka"]);
            row.push(hinbanRow["suryo"]);
            row.push(hinbanRow["Goukei"]);

            table.push(row);
        }
        return table;
    }















    return {
        initializeDialoge: initializeDialoge,
    };
})();
