export class Constants {

    static readonly LOADING_DATA = "処理中..."

    static readonly SIHARAISAKI_NOT_RECORD = "支払先コードが入力されていません。";
    static readonly SYUKEIHI_NOT_RECORD = "支払先コードが入力されていません。";
    static readonly SHOULD_ZERO_ALLOWED = "0の値を税率に設定します。よろしいですか。";
}

