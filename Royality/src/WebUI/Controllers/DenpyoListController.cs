﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Royality.Application.DenpyoItems.Queries.GetDenpyoInfoList;
using Royality.Application.EnumItems.Queries.GetStatusTypeList;
using Royality.Application.SiharaiSakiItems.Queries;
using Royality.Application.SiharaiSakiItems.Queries.GetSiireSakiList;
using Royality.Models;
using Royality.WebUI.Controllers;
using Royality.WebUI.Utility;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Royality.Controllers
{
    public class DenpyoListController : BaseController
    {
        #region Constructur
        public DenpyoListController(IMediator medidator) : base(medidator)
        {
        }
        #endregion


        public async Task<IActionResult> Index()
        {
            DenpyoListViewModel objViewModel = new DenpyoListViewModel();

            var responseStatusList = await _mediator.Send(new GetStatusTypeListQuery());
            objViewModel.StatusList = WebUtility.CreateSelectList(responseStatusList);

            return View(objViewModel);
        }

        /// <summary>
        /// テーブル表示用の伝票リストを取得
        /// </summary>
        /// <param name="searchData"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GetDenpyoList([FromForm] GetDenpyoInfoListQuery searchData)
        {
            DenpyoInfoList resDenListQuery = await _mediator.Send(searchData);

            var response = new
            {
                Result = resDenListQuery.DenpyoList,
                resDenListQuery.Draw,
                resDenListQuery.RecordsTotal,
                resDenListQuery.RecordsFiltered
            };

            return new JsonResult(response);
        }
            
        public async Task<IActionResult> SiharaiSakiDlg()
        {
            List<Siharaisaki> response = await _mediator.Send(new GetSiharaiSakiListQuery());
            return PartialView("~/Views/CommonDialog/_DlgSiharaisakiList.cshtml", response);
        }
    }
}