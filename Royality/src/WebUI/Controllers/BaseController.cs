﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Royality.WebUI.Controllers
{
    public abstract class BaseController : Controller
    {
        #region Private Member
        protected readonly IMediator _mediator;
        #endregion

        #region Controller
        /// <summary>
        /// Controller
        /// </summary>
        /// <param name="mediator"></param>
        public BaseController(IMediator mediator)
        {
            _mediator = mediator;
        }
        #endregion
    }
}
