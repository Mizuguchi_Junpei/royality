﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Royality.Application.DenpyoItems.Commands.SaveDenpyo;
using Royality.Application.DenpyoItems.Queries.GetDenpyoInfo;
using Royality.WebUI.Models;
using System.Threading.Tasks;

namespace Royality.WebUI.Controllers
{
    public class DenpyoDetailController : BaseController
    {
        #region Constructor
        public DenpyoDetailController(IMediator mediator) : base(mediator)
        {
        }
        #endregion

        public async Task<IActionResult> Index(string id)
        {
            DenDetailInfoViewModel DetailViewModel = new DenDetailInfoViewModel();

            DenpyoInfo responseDenQuery = await _mediator.Send(new GetDenpyoInfoQuery() { DenNo = id });
            DetailViewModel.DenpyoInfo = responseDenQuery;

            ///モデル　鮮度
            ///
            ///ビューを返す

            return View(DetailViewModel);
        }

        public async Task<IActionResult> Save(SaveDenpyoInfo denpyoInfo)
        {
            var resDenNo = await _mediator.Send(new SaveDenpyoCommand() { DenpyoInfo = denpyoInfo });
            return new JsonResult(new Response { Result = resDenNo });
        }
    }
}
