﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Royailty.Application.AnbunItems.Commands;
using Royality.Application.AnbunItems.Queries;
using Royality.Application.AnbunItems.Queries.GetDenpyoInfoUriageList;
using Royality.Application.Common.Models;
using Royality.Application.DenpyoItems.Queries.GetDenpyoInfo;
using Royality.WebUI.Models;
using System;
using System.Threading.Tasks;

namespace Royality.WebUI.Controllers
{
    public class HinbanTourokuDialogController : BaseController
    {
        public HinbanTourokuDialogController(IMediator mediator) : base(mediator)
        {
        }

        public class AnbunViewModel
        {
            public DenpyoInfo DenpyoInfo { set; get; }
        }

        public async Task<IActionResult> Index()
        {
            return PartialView("~/Views/DenpyoDetail/Dialog/_DigHinbanTouroku.cshtml");
        }

        /// <summary>
        /// Get Denpyo List Partial View
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GetDenpyoUriage([FromForm] GetDenpyoInfoListUriageQuery uriageData)
        {
            DenpyoInfoListUriage resDenListUriageQuery = await _mediator.Send(uriageData);

            var response = new
            {
                Result = resDenListUriageQuery.DenpyoList,
                resDenListUriageQuery.ImpRecordsTotal,
                resDenListUriageQuery.ImpRecordsFiltered,
            };
            return new JsonResult(response);
        }

        /// <summary>
        /// 按分計算
        /// </summary>
        /// <param name="anbunSettings"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AutoAnbunKeisan(AutoAnbunViewModel anbunSettings)
        {
            try
            {
                AnbunKeisanCommand anbunQuery = new AnbunKeisanCommand()
                {
                    AnbunSetting = anbunSettings.AnbunSetting,
                    UriageInfoList = anbunSettings.UriageInfoList

                };
                await _mediator.Send(anbunQuery);

                AnbunResult objAnbunResult = await _mediator.Send(new GetAnbunResultQuery() { DenNo = anbunSettings.AnbunSetting.DenNo });
                return new JsonResult(new Response { Result = objAnbunResult });
            }
            catch (Exception e)
            {
                return PartialView("~/Views/DenpyoDetail/Dialog/_DigHinbanTouroku.cshtml");
            }
        }
    }
}
