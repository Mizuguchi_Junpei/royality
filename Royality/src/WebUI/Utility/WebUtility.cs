﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;

namespace Royality.WebUI.Utility
{
    public static class WebUtility
    {
        /// <summary>
        /// Is Active
        /// </summary>
        /// <param name="httpRequest"></param>
        /// <param name="control"></param>
        /// <returns></returns>
        public static string IsActive(HttpRequest httpRequest, string control, string action)
        {
            string routeControl = (string)httpRequest.RouteValues["controller"];

            bool returnActive = control == routeControl;

            return returnActive ? "active" : "";
        }

        public static SelectList CreateSelectList(List<SelectListItem> selectListItem)
        {
            ///nullだったらList<SelectListItem>()を代入
            selectListItem ??= new List<SelectListItem>();
            selectListItem.Insert(0, new SelectListItem("未選択", "0"));

            SelectList objSelectList = new SelectList(selectListItem, "Value", "Text", "0");
            return objSelectList;
        }

    }
}
