﻿using Royality.Application.Common.Models;
using System.Collections.Generic;

namespace Royality.WebUI.Models
{
    public class AutoAnbunViewModel
    {
        public AnbunSetting AnbunSetting { get; set; }
        public List<UriageInfo> UriageInfoList { get; set; }
    }
}
