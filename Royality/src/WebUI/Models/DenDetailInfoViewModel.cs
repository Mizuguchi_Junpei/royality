﻿using Royality.Application.DenpyoItems.Queries.GetDenpyoInfo;

namespace Royality.WebUI.Models
{
    /// <summary>
    /// Den Detail Info
    /// </summary>
    public class DenDetailInfoViewModel
    {
        #region Property
        public DenpyoInfo DenpyoInfo { get; set; }
        public int? HinbanCount { get; set; }
        public bool? IsAnbunResultSet { get; set; }
        public bool IsNewDenpyo { get; set; }
        #endregion
    }
}
