﻿using Microsoft.AspNetCore.Mvc.Rendering;

namespace Royality.Models
{
    public class DenpyoListViewModel
    {
        #region Property
        public SelectList StatusList { get; set; }
        #endregion
    }
}
