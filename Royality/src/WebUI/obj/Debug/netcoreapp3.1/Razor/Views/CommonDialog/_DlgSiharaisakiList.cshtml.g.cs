#pragma checksum "C:\work\Right-on\ロイヤリティ\Royality\src\WebUI\Views\CommonDialog\_DlgSiharaisakiList.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "08cb0b0722d17763da72e2f2d773e96090b2b2bf"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_CommonDialog__DlgSiharaisakiList), @"mvc.1.0.view", @"/Views/CommonDialog/_DlgSiharaisakiList.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\work\Right-on\ロイヤリティ\Royality\src\WebUI\Views\_ViewImports.cshtml"
using Royality.WebUI;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\work\Right-on\ロイヤリティ\Royality\src\WebUI\Views\_ViewImports.cshtml"
using Royality.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\work\Right-on\ロイヤリティ\Royality\src\WebUI\Views\_ViewImports.cshtml"
using Royality.WebUI.Utility;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\work\Right-on\ロイヤリティ\Royality\src\WebUI\Views\_ViewImports.cshtml"
using Royality.Application.DenpyoItems.Queries.GetDenpyoInfo;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"08cb0b0722d17763da72e2f2d773e96090b2b2bf", @"/Views/CommonDialog/_DlgSiharaisakiList.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6986ff224ac0fdf70c4668c3e208b9cfdd8fe84c", @"/Views/_ViewImports.cshtml")]
    public class Views_CommonDialog__DlgSiharaisakiList : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
#nullable restore
#line 1 "C:\work\Right-on\ロイヤリティ\Royality\src\WebUI\Views\CommonDialog\_DlgSiharaisakiList.cshtml"
Write(Model);

#line default
#line hidden
#nullable disable
            WriteLiteral(@" IEnumerable<Siharaisaki>

    <!-- _/_/_/_/_/_/_/_/_/_/_/_/ -->
    <!-- _/  検索子画面           -->
    <!-- _/_/_/_/_/_/_/_/_/_/_/_/ -->
    <!-- 仕入先検索 -->
    <div class=""modal-dialog"">
        <div class=""modal-content"">
            <div class=""modal-header"">
                <button type=""button"" class=""close"" data-dismiss=""modal""><span>x</span></button>
                <h4 class="" "">支払先検索</h4>
            </div>
            <div class=""modal-body"">
                <table class=""table table-striped table-hover item_center"">
                    <thead class=""scrl_head"">
                        <tr>
                            <th class=""siharaisaki_code"">支払先コード</th>
                            <th class=""siharaisaki_name"">支払先名</th>
                        </tr>
                    </thead>
                    <tbody class=""scrl_body"">
                        <!--- 支払先リスト --->
");
#nullable restore
#line 23 "C:\work\Right-on\ロイヤリティ\Royality\src\WebUI\Views\CommonDialog\_DlgSiharaisakiList.cshtml"
                         foreach (var siharaisaki in Model)
                        {
                            var id = "sname_" + @siharaisaki.Code;
                            

#line default
#line hidden
#nullable disable
            WriteLiteral("<tr");
            BeginWriteAttribute("onclick", " onclick=\"", 1104, "\"", 1174, 6);
            WriteAttributeValue("", 1114, "siharaisaki_select(\'", 1114, 20, true);
#nullable restore
#line 26 "C:\work\Right-on\ロイヤリティ\Royality\src\WebUI\Views\CommonDialog\_DlgSiharaisakiList.cshtml"
WriteAttributeValue("", 1134, siharaisaki.Code, 1134, 17, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1151, "\',", 1151, 2, true);
            WriteAttributeValue(" ", 1153, "\'", 1154, 2, true);
#nullable restore
#line 26 "C:\work\Right-on\ロイヤリティ\Royality\src\WebUI\Views\CommonDialog\_DlgSiharaisakiList.cshtml"
WriteAttributeValue("", 1155, siharaisaki.Name, 1155, 17, false);

#line default
#line hidden
#nullable disable
            WriteAttributeValue("", 1172, "\')", 1172, 2, true);
            EndWriteAttribute();
            WriteLiteral(">\r\n                                <td class=\"siharaisaki_code\">");
#nullable restore
#line 27 "C:\work\Right-on\ロイヤリティ\Royality\src\WebUI\Views\CommonDialog\_DlgSiharaisakiList.cshtml"
                                                        Write(siharaisaki.Code);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                                <td class=\"siharaisaki_name\"");
            BeginWriteAttribute("id", " id=\"", 1323, "\"", 1331, 1);
#nullable restore
#line 28 "C:\work\Right-on\ロイヤリティ\Royality\src\WebUI\Views\CommonDialog\_DlgSiharaisakiList.cshtml"
WriteAttributeValue("", 1328, id, 1328, 3, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">");
#nullable restore
#line 28 "C:\work\Right-on\ロイヤリティ\Royality\src\WebUI\Views\CommonDialog\_DlgSiharaisakiList.cshtml"
                                                                 Write(siharaisaki.Name);

#line default
#line hidden
#nullable disable
            WriteLiteral("</td>\r\n                            </tr>\r\n");
#nullable restore
#line 30 "C:\work\Right-on\ロイヤリティ\Royality\src\WebUI\Views\CommonDialog\_DlgSiharaisakiList.cshtml"
                        }

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                        <!--- 支払先リスト --->
                    </tbody>
                </table>
            </div>
            <div class=""modal-footer"">
                <button type=""button"" class=""btn btn-default"" data-dismiss=""modal"">閉じる</button>
");
            WriteLiteral("            </div>\r\n        </div>\r\n    </div>\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
