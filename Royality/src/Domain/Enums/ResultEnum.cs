﻿namespace Royality.Domain.Enums
{
    public enum ResultEnum
    {
        Done = 1,
        NotDone = 2
    }
}
