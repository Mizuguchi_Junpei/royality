﻿namespace Royality.Domain.Enums
{
    public enum StatusEnum
    {
        None = 0,
        Saved = 1,    //保存
        Confirm = 2,  //確定
        Applied = 3,  //申請済
    }
}
