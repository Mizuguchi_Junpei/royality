﻿using Royality.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace Royality.Domain.Common
{
    public class Helpers
    {
        public static Dictionary<StatusEnum, string> StatusValuePair = new Dictionary<StatusEnum, string>()
        {
            {StatusEnum.Saved, "保存" },
            {StatusEnum.Confirm, "確定" },
            {StatusEnum.Applied, "申請" },
        };

        public static Dictionary<ResultEnum, string> ResultValuePair = new Dictionary<ResultEnum, string>()
        {
            {ResultEnum.Done, "済" },
            {ResultEnum.NotDone, "未" },
        };
    }
}
