﻿namespace Royality.Domain.Common
{
    public class Constants
    {
        public static readonly string DISPLAY_DATE_FORMAT = "yyyy-MM-dd";
        public static readonly byte DEFAULT_DEN_TAX = 10;
    }
}
