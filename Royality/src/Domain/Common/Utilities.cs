﻿using System;
using System.Globalization;

namespace Royality.Domain.Common
{
    public static class Utilities
    {
        #region Public Readonly
        public static readonly char DirectorySeparatorChar = '\\';
        public static readonly char AltDirectorySeparatorChar = '/';
        public static readonly char VolumeSeparatorChar = ':';
        #endregion

        #region Public Method

        /// <summary>
        /// Convert DateTime object to string
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string ConvertToDateTime(DateTime dateTime)
        {
            string sDateTime = dateTime.ToString(Constants.DISPLAY_DATE_FORMAT);
            return sDateTime;
        }

        /// <summary>
        /// Convert Nullable DateTime object to string
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static string ConvertToDateTime(DateTime? dateTime)
        {
            string sDateTime = null;
            if(dateTime != null)
            {
                sDateTime = ((DateTime)dateTime).ToString(Constants.DISPLAY_DATE_FORMAT);
            }
            return sDateTime;
        }

        /// <summary>
        /// Convert to Nullable Date Time
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public static DateTime? ConvertToDateTimeNullable(string dateTime)
        {
            DateTime? dtDateTime = null;
            if (!string.IsNullOrWhiteSpace(dateTime))
            {
                dtDateTime = DateTime.ParseExact(dateTime, Constants.DISPLAY_DATE_FORMAT, CultureInfo.InvariantCulture);
            }
            return dtDateTime;
        }


        #endregion
    }
}
