﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class TDayTenSkuZaiko
    {
        public DateTime 運用日 { get; set; }
        public string 店舗コード { get; set; }
        public string Skuコード { get; set; }
        public string 買取受託 { get; set; }
        public int 在庫数量 { get; set; }
    }
}
