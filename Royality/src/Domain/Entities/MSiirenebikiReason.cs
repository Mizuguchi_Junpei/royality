﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class MSiirenebikiReason
    {
        public string ReasonCd { get; set; }
        public byte ApportionType { get; set; }
        public byte ApportionSetting { get; set; }
        public string ReasonDetail { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
