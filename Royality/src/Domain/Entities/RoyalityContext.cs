﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Domain.Entities
{
    public partial class RoyalityContext : DbContext
    {
        public RoyalityContext()
        {
        }

        public RoyalityContext(DbContextOptions<RoyalityContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DSiireDenpyouMeisai> DSiireDenpyouMeisai { get; set; }
        public virtual DbSet<DUriageDenpyouMeisai> DUriageDenpyouMeisai { get; set; }
        public virtual DbSet<MBumonMaster> MBumonMaster { get; set; }
        public virtual DbSet<MDenpyouSimeMaster> MDenpyouSimeMaster { get; set; }
        public virtual DbSet<MGenkaRireki> MGenkaRireki { get; set; }
        public virtual DbSet<MGyoutaiMaster> MGyoutaiMaster { get; set; }
        public virtual DbSet<MShouhinData> MShouhinData { get; set; }
        public virtual DbSet<MSiirenebikiReason> MSiirenebikiReason { get; set; }
        public virtual DbSet<MSiiresakiMaster> MSiiresakiMaster { get; set; }
        public virtual DbSet<MTenpoMaster> MTenpoMaster { get; set; }
        public virtual DbSet<MUser> MUser { get; set; }
        public virtual DbSet<MiSysCalendarInfo> MiSysCalendarInfo { get; set; }
        public virtual DbSet<TAnbunResultUriage> TAnbunResultUriage { get; set; }
        public virtual DbSet<TAnbunResultUriageAuto> TAnbunResultUriageAuto { get; set; }
        public virtual DbSet<TDayTenSkuZaiko> TDayTenSkuZaiko { get; set; }
        public virtual DbSet<TDenNo> TDenNo { get; set; }
        public virtual DbSet<TRoyalityAnbun> TRoyalityAnbun { get; set; }
        public virtual DbSet<TRoyalityBrand> TRoyalityBrand { get; set; }
        public virtual DbSet<TRoyalityDen> TRoyalityDen { get; set; }
        public virtual DbSet<TRoyalityDenMeisai> TRoyalityDenMeisai { get; set; }
        public virtual DbSet<TRoyalityDenUriage> TRoyalityDenUriage { get; set; }
        public virtual DbSet<TRoyalityPayee> TRoyalityPayee { get; set; }
        public virtual DbSet<TSiirenebikiDen> TSiirenebikiDen { get; set; }
        public virtual DbSet<TSiirenebikiDenAnbun> TSiirenebikiDenAnbun { get; set; }
        public virtual DbSet<TSiirenebikiDenHinban> TSiirenebikiDenHinban { get; set; }
        public virtual DbSet<TSiirenebikiDenMeisai> TSiirenebikiDenMeisai { get; set; }
        public virtual DbSet<TSiirenebikiDenTenpo> TSiirenebikiDenTenpo { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=127.0.0.1;Database=Royality;Trusted_Connection=True;User Id=sa;Password=Sa@01;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DSiireDenpyouMeisai>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("D_SIIRE_DENPYOU_MEISAI");

                entity.Property(e => e.Skuコード)
                    .IsRequired()
                    .HasColumnName("SKUコード")
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.トランゼクションid).HasColumnName("トランゼクションID");

                entity.Property(e => e.仕入伝票日付).HasColumnType("date");

                entity.Property(e => e.仕入伝票明細番号)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.仕入伝票番号)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.仕入先コード)
                    .IsRequired()
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.勘定コード)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.店舗コード)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.発生日).HasColumnType("date");

                entity.Property(e => e.登録時刻).HasColumnType("datetime");

                entity.Property(e => e.税コード)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.訂正元購買伝票番号)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.買取受託)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<DUriageDenpyouMeisai>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("D_URIAGE_DENPYOU_MEISAI");

                entity.Property(e => e.Skuコード)
                    .HasColumnName("SKUコード")
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.トランゼクションid).HasColumnName("トランゼクションID");

                entity.Property(e => e.仕入先コード)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.免税)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.勘定コード)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.売上伝票日付).HasColumnType("date");

                entity.Property(e => e.売上伝票明細番号)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.売上伝票番号)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.店舗コード)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.発生日).HasColumnType("date");

                entity.Property(e => e.登録日時).HasColumnType("datetime");

                entity.Property(e => e.税コード)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.買取受託)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MBumonMaster>(entity =>
            {
                entity.HasKey(e => new { e.部門コード, e.クラスコード });

                entity.ToTable("M_BUMON_MASTER");

                entity.Property(e => e.部門コード)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.クラスコード)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.クラス名)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.更新日).HasColumnType("datetime");

                entity.Property(e => e.服種コード)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.服種名)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.業態コード)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.業態名)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.部門名)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MDenpyouSimeMaster>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("M_DENPYOU_SIME_MASTER");

                entity.Property(e => e.ロイヤリティ締日).HasColumnType("date");

                entity.Property(e => e.会計締日).HasColumnType("date");

                entity.Property(e => e.年度)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.月度)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.月度終了日).HasColumnType("date");

                entity.Property(e => e.月度締日).HasColumnType("date");

                entity.Property(e => e.月度開始日).HasColumnType("date");

                entity.Property(e => e.登録日時).HasColumnType("datetime");
            });

            modelBuilder.Entity<MGenkaRireki>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("M_GENKA_RIREKI");

                entity.Property(e => e.更新日).HasColumnType("datetime");

                entity.Property(e => e.有効終了日).HasColumnType("date");

                entity.Property(e => e.有効開始日).HasColumnType("date");

                entity.Property(e => e.Ｓｋｕコード)
                    .IsRequired()
                    .HasColumnName("ＳＫＵコード")
                    .HasMaxLength(13)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MGyoutaiMaster>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("M_GYOUTAI_MASTER");

                entity.Property(e => e.業態コード)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.業態区分)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.業態名)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MShouhinData>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("M_SHOUHIN_DATA");

                entity.Property(e => e.アーカイブフラグ)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.カラー)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.クラスコード)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.サイズ)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.シーズンコード)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.シーズン年度)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.ブランドコード)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.メーカー品番)
                    .IsRequired()
                    .HasMaxLength(18);

                entity.Property(e => e.仕入先コード)
                    .IsRequired()
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.削除フラグ)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.商品コード)
                    .IsRequired()
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.商品テキスト)
                    .IsRequired()
                    .HasMaxLength(40);

                entity.Property(e => e.引き取り期限日).HasColumnType("date");

                entity.Property(e => e.更新日).HasColumnType("datetime");

                entity.Property(e => e.有効開始日).HasColumnType("date");

                entity.Property(e => e.服種コード)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.販売終了日).HasColumnType("date");

                entity.Property(e => e.販売開始日).HasColumnType("date");

                entity.Property(e => e.部門コード)
                    .IsRequired()
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ＮｂＰｂ区分)
                    .HasColumnName("ＮＢ/ＰＢ区分")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.Ｓｋｕコード)
                    .IsRequired()
                    .HasColumnName("ＳＫＵコード")
                    .HasMaxLength(13)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MSiirenebikiReason>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("M_SIIRENEBIKI_REASON");

                entity.Property(e => e.ApportionSetting).HasColumnName("APPORTION_SETTING");

                entity.Property(e => e.ApportionType).HasColumnName("APPORTION_TYPE");

                entity.Property(e => e.ReasonCd)
                    .IsRequired()
                    .HasColumnName("REASON_CD")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ReasonDetail)
                    .HasColumnName("REASON_DETAIL")
                    .HasMaxLength(50);

                entity.Property(e => e.Timestamp)
                    .HasColumnName("TIMESTAMP")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<MSiiresakiMaster>(entity =>
            {
                entity.HasKey(e => e.仕入先コード);

                entity.ToTable("M_SIIRESAKI_MASTER");

                entity.Property(e => e.仕入先コード)
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.仕入先名).HasMaxLength(100);

                entity.Property(e => e.更新日).HasColumnType("datetime");

                entity.Property(e => e.登記住所１).HasMaxLength(100);

                entity.Property(e => e.登記住所２).HasMaxLength(100);

                entity.Property(e => e.返品住所１).HasMaxLength(100);

                entity.Property(e => e.返品住所２).HasMaxLength(100);

                entity.Property(e => e.郵便番号)
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.郵便番号返品)
                    .HasColumnName("郵便番号(返品)")
                    .HasMaxLength(8)
                    .IsUnicode(false);

                entity.Property(e => e.電話番号)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.電話番号返品)
                    .HasColumnName("電話番号(返品)")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Ｆａｘ番号)
                    .HasColumnName("ＦＡＸ番号")
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Ｆａｘ番号返品)
                    .HasColumnName("ＦＡＸ番号(返品)")
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MTenpoMaster>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("M_TENPO_MASTER");

                entity.Property(e => e.エリアコード)
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.エリア名).HasMaxLength(100);

                entity.Property(e => e.オープン日).HasColumnType("date");

                entity.Property(e => e.ブロックコード)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.ブロック名).HasMaxLength(100);

                entity.Property(e => e.プレオープン日).HasColumnType("date");

                entity.Property(e => e.事業部コード)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.事業部名).HasMaxLength(100);

                entity.Property(e => e.住所１).HasMaxLength(150);

                entity.Property(e => e.住所２).HasMaxLength(150);

                entity.Property(e => e.削除フラグ)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.契約区分).HasMaxLength(50);

                entity.Property(e => e.契約区分コード)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.店舗コード)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.店舗名称).HasMaxLength(100);

                entity.Property(e => e.店舗略式名称).HasMaxLength(100);

                entity.Property(e => e.形態区分).HasMaxLength(20);

                entity.Property(e => e.形態区分コード)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.搬入開始日).HasColumnType("date");

                entity.Property(e => e.更新日).HasColumnType("datetime");

                entity.Property(e => e.有効開始日).HasColumnType("date");

                entity.Property(e => e.経年区分)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.経年区分コード)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.郵便番号)
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.Property(e => e.都道府県).HasMaxLength(50);

                entity.Property(e => e.都道府県コード)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.閉店日).HasColumnType("date");

                entity.Property(e => e.電話番号)
                    .HasMaxLength(15)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<MUser>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("M_USER");

                entity.Property(e => e.Timestamp)
                    .HasColumnName("TIMESTAMP")
                    .HasColumnType("datetime");

                entity.Property(e => e.UserCd)
                    .IsRequired()
                    .HasColumnName("USER_CD")
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.UserName)
                    .IsRequired()
                    .HasColumnName("USER_NAME")
                    .HasMaxLength(20);
            });

            modelBuilder.Entity<MiSysCalendarInfo>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("MI_SYS_CALENDAR_INFO");

                entity.Property(e => e.カレンダー年月日).HasColumnType("date");

                entity.Property(e => e.会計期)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.削除区分)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.前年同曜日).HasColumnType("date");

                entity.Property(e => e.前年同週)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.前年年度)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.前年日).HasColumnType("date");

                entity.Property(e => e.前年曜日コード)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.前年月度)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.前年祝日区分)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.前年週度)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.前年週度用年度)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.平日休日区分)
                    .HasColumnName("平日_休日区分")
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.年度)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.曜日コード)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.更新日).HasColumnType("datetime");

                entity.Property(e => e.更新者)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.月度)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.月度内何週目).HasColumnType("numeric(1, 0)");

                entity.Property(e => e.月度ｓｅｑ)
                    .HasColumnName("月度ＳＥＱ")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.期)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.祝日区分)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.祝日名称).HasMaxLength(20);

                entity.Property(e => e.週度)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.週度期間)
                    .HasColumnName("週度_期間")
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.週度用会計期)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Ｓａｌｅ名称１)
                    .HasColumnName("ＳＡＬＥ名称１")
                    .HasMaxLength(100);

                entity.Property(e => e.Ｓａｌｅ名称２)
                    .HasColumnName("ＳＡＬＥ名称２")
                    .HasMaxLength(100);

                entity.Property(e => e.Ｓａｌｅ名称３)
                    .HasColumnName("ＳＡＬＥ名称３")
                    .HasMaxLength(100);
            });

            modelBuilder.Entity<TAnbunResultUriage>(entity =>
            {
                entity.HasKey(e => new { e.DenpyouNo, e.TenpoCode, e.Sku });

                entity.ToTable("T_ANBUN_RESULT_URIAGE");

                entity.Property(e => e.DenpyouNo)
                    .HasColumnName("DENPYOU_NO")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.TenpoCode)
                    .HasColumnName("TENPO_CODE")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Sku)
                    .HasColumnName("SKU")
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.Amt).HasColumnName("AMT");

                entity.Property(e => e.Qty).HasColumnName("QTY");

                entity.Property(e => e.Ritsu)
                    .HasColumnName("RITSU")
                    .HasColumnType("decimal(18, 0)");
            });

            modelBuilder.Entity<TAnbunResultUriageAuto>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("T_ANBUN_RESULT_URIAGE_AUTO");

                entity.Property(e => e.Skuコード)
                    .IsRequired()
                    .HasColumnName("SKUコード")
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.仕入値引伝票番号)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.値引合計金額).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.値引按分金額).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.店舗コード)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.按分差額).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.按分率).HasColumnType("decimal(25, 13)");

                entity.Property(e => e.自社品番)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.調整後按分残).HasColumnType("decimal(38, 0)");

                entity.Property(e => e.調整後按分額).HasColumnType("decimal(38, 0)");
            });

            modelBuilder.Entity<TDayTenSkuZaiko>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("T_DAY_TEN_SKU_ZAIKO");

                entity.Property(e => e.Skuコード)
                    .IsRequired()
                    .HasColumnName("SKUコード")
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.店舗コード)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.買取受託)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.運用日).HasColumnType("date");
            });

            modelBuilder.Entity<TDenNo>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("T_DEN_NO");

                entity.Property(e => e.伝票番号)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<TRoyalityAnbun>(entity =>
            {
                entity.HasKey(e => new { e.伝票番号, e.店舗コード, e.自社品番, e.Sku });

                entity.ToTable("T_ROYALITY_ANBUN");

                entity.Property(e => e.伝票番号)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.店舗コード)
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.自社品番)
                    .HasMaxLength(13)
                    .IsFixedLength();

                entity.Property(e => e.Sku)
                    .HasColumnName("SKU")
                    .HasMaxLength(13)
                    .IsFixedLength();

                entity.Property(e => e.カラー)
                    .HasMaxLength(4)
                    .IsFixedLength();

                entity.Property(e => e.ブランド).HasMaxLength(6);

                entity.Property(e => e.メーカー品番)
                    .HasMaxLength(20)
                    .IsFixedLength();

                entity.Property(e => e.売上伝票番号)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.部門)
                    .HasMaxLength(20)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.部門別シェア率).HasColumnType("decimal(11, 10)");
            });

            modelBuilder.Entity<TRoyalityBrand>(entity =>
            {
                entity.HasKey(e => new { e.支払先コード, e.ブランドコード });

                entity.ToTable("T_ROYALITY_BRAND");

                entity.Property(e => e.支払先コード)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.ブランドコード).HasMaxLength(6);
            });

            modelBuilder.Entity<TRoyalityDen>(entity =>
            {
                entity.HasKey(e => e.伝票番号);

                entity.ToTable("T_ROYALITY_DEN");

                entity.Property(e => e.伝票番号)
                    .HasMaxLength(10)
                    .IsFixedLength();

                entity.Property(e => e.伝票日付).HasColumnType("date");

                entity.Property(e => e.備考).HasMaxLength(200);

                entity.Property(e => e.支払先コード)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.更新ユーザー)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.更新日時).HasColumnType("datetime");
            });

            modelBuilder.Entity<TRoyalityDenMeisai>(entity =>
            {
                entity.HasKey(e => new { e.伝票番号, e.部門コード });

                entity.ToTable("T_ROYALITY_DEN_MEISAI");

                entity.Property(e => e.伝票番号)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.部門コード)
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.品名).HasMaxLength(20);

                entity.Property(e => e.更新ユーザー)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.更新日時).HasColumnType("datetime");
            });

            modelBuilder.Entity<TRoyalityDenUriage>(entity =>
            {
                entity.HasKey(e => new { e.DenpyoNo, e.自社品番 })
                    .HasName("PK_Table_1");

                entity.ToTable("T_ROYALITY_DEN_URIAGE");

                entity.Property(e => e.DenpyoNo)
                    .HasColumnName("DENPYO_NO")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.自社品番)
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.メーカー品番)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TRoyalityPayee>(entity =>
            {
                entity.HasKey(e => e.支払先コード);

                entity.ToTable("T_ROYALITY_PAYEE");

                entity.Property(e => e.支払先コード)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.支払先名).HasMaxLength(20);
            });

            modelBuilder.Entity<TSiirenebikiDen>(entity =>
            {
                entity.HasKey(e => e.伝票番号);

                entity.ToTable("T_SIIRENEBIKI_DEN");

                entity.Property(e => e.伝票番号)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.仕入先コード)
                    .IsRequired()
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.Property(e => e.伝票日付).HasColumnType("date");

                entity.Property(e => e.備考).HasMaxLength(200);

                entity.Property(e => e.削除flg).HasColumnName("削除FLG");

                entity.Property(e => e.店舗コード)
                    .IsRequired()
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.更新ユーザー)
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.更新日時).HasColumnType("datetime");

                entity.Property(e => e.業態コード)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.理由コード)
                    .IsRequired()
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .IsFixedLength();
            });

            modelBuilder.Entity<TSiirenebikiDenAnbun>(entity =>
            {
                entity.HasKey(e => e.DenpyouNo);

                entity.ToTable("T_SIIRENEBIKI_DEN_ANBUN");

                entity.Property(e => e.DenpyouNo)
                    .HasColumnName("DENPYOU_NO")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.Anbunsett).HasColumnName("ANBUNSETT");

                entity.Property(e => e.Method).HasColumnName("METHOD");

                entity.Property(e => e.Target).HasColumnName("TARGET");

                entity.Property(e => e.TermFrom)
                    .HasColumnName("TERM_FROM")
                    .HasColumnType("datetime");

                entity.Property(e => e.TermTo)
                    .HasColumnName("TERM_TO")
                    .HasColumnType("datetime");
            });

            modelBuilder.Entity<TSiirenebikiDenHinban>(entity =>
            {
                entity.HasKey(e => new { e.DenpyouNo, e.自社品番 });

                entity.ToTable("T_SIIRENEBIKI_DEN_HINBAN");

                entity.Property(e => e.DenpyouNo)
                    .HasColumnName("DENPYOU_NO")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.自社品番)
                    .HasMaxLength(13)
                    .IsUnicode(false);

                entity.Property(e => e.メーカー品番)
                    .IsRequired()
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TSiirenebikiDenMeisai>(entity =>
            {
                entity.HasKey(e => new { e.順番番号, e.伝票番号, e.部門コード, e.クラスコード });

                entity.ToTable("T_SIIRENEBIKI_DEN_MEISAI");

                entity.Property(e => e.伝票番号)
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.部門コード)
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.クラスコード)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.品名)
                    .HasMaxLength(30)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<TSiirenebikiDenTenpo>(entity =>
            {
                entity.HasKey(e => new { e.DenpyoNo, e.店舗コード });

                entity.ToTable("T_SIIRENEBIKI_DEN_TENPO");

                entity.Property(e => e.DenpyoNo)
                    .HasColumnName("DENPYO_NO")
                    .HasMaxLength(10)
                    .IsUnicode(false)
                    .IsFixedLength();

                entity.Property(e => e.店舗コード)
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.店舗名称).HasMaxLength(100);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
