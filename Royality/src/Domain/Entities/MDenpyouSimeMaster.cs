﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class MDenpyouSimeMaster
    {
        public string 年度 { get; set; }
        public string 月度 { get; set; }
        public DateTime 月度開始日 { get; set; }
        public DateTime 月度終了日 { get; set; }
        public DateTime? 月度締日 { get; set; }
        public DateTime? ロイヤリティ締日 { get; set; }
        public DateTime? 会計締日 { get; set; }
        public DateTime 登録日時 { get; set; }
    }
}
