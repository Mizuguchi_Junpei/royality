﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class TSiirenebikiDenAnbun
    {
        public string DenpyouNo { get; set; }
        public byte Anbunsett { get; set; }
        public byte Method { get; set; }
        public DateTime? TermFrom { get; set; }
        public DateTime? TermTo { get; set; }
        public byte? Target { get; set; }
    }
}
