﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class DUriageDenpyouMeisai
    {
        public DateTime 売上伝票日付 { get; set; }
        public string 売上伝票番号 { get; set; }
        public string 売上伝票明細番号 { get; set; }
        public string 買取受託 { get; set; }
        public string 勘定コード { get; set; }
        public string 仕入先コード { get; set; }
        public string 店舗コード { get; set; }
        public string Skuコード { get; set; }
        public int? 売上数 { get; set; }
        public long? 売上原価 { get; set; }
        public long? 売上売価 { get; set; }
        public long? 売上割引 { get; set; }
        public long 税額 { get; set; }
        public string 免税 { get; set; }
        public long トランゼクションid { get; set; }
        public string 税コード { get; set; }
        public DateTime 発生日 { get; set; }
        public DateTime 登録日時 { get; set; }
    }
}
