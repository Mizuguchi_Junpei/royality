﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class MSiiresakiMaster
    {
        public string 仕入先コード { get; set; }
        public string 仕入先名 { get; set; }
        public string 登記住所１ { get; set; }
        public string 登記住所２ { get; set; }
        public string 郵便番号 { get; set; }
        public string 電話番号 { get; set; }
        public string Ｆａｘ番号 { get; set; }
        public string 返品住所１ { get; set; }
        public string 返品住所２ { get; set; }
        public string 郵便番号返品 { get; set; }
        public string 電話番号返品 { get; set; }
        public string Ｆａｘ番号返品 { get; set; }
        public DateTime? 更新日 { get; set; }
    }
}
