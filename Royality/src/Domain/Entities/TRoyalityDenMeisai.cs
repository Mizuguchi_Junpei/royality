﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class TRoyalityDenMeisai
    {
        public string 伝票番号 { get; set; }
        public string 部門コード { get; set; }
        public string 品名 { get; set; }
        public int? 金額 { get; set; }
        public int? 端数処理後金額 { get; set; }
        public bool? 削除フラグ { get; set; }
        public string 更新ユーザー { get; set; }
        public DateTime? 更新日時 { get; set; }
    }
}
