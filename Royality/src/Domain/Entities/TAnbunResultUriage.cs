﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class TAnbunResultUriage
    {
        public string DenpyouNo { get; set; }
        public string TenpoCode { get; set; }
        public string Sku { get; set; }
        public int Qty { get; set; }
        public int Amt { get; set; }
        public decimal? Ritsu { get; set; }
    }
}
