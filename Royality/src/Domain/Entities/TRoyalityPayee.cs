﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class TRoyalityPayee
    {
        public string 支払先コード { get; set; }
        public string 支払先名 { get; set; }
    }
}
