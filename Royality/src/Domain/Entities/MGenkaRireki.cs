﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class MGenkaRireki
    {
        public string Ｓｋｕコード { get; set; }
        public long? 購買価格 { get; set; }
        public DateTime 有効開始日 { get; set; }
        public DateTime 有効終了日 { get; set; }
        public DateTime? 更新日 { get; set; }
    }
}
