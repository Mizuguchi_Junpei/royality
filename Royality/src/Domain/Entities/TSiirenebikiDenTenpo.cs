﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class TSiirenebikiDenTenpo
    {
        public string DenpyoNo { get; set; }
        public string 店舗コード { get; set; }
        public string 店舗名称 { get; set; }
    }
}
