﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class MiSysCalendarInfo
    {
        public DateTime カレンダー年月日 { get; set; }
        public string 曜日コード { get; set; }
        public string 祝日区分 { get; set; }
        public string 年度 { get; set; }
        public string 会計期 { get; set; }
        public string 期 { get; set; }
        public string 月度 { get; set; }
        public string 週度 { get; set; }
        public string 月度ｓｅｑ { get; set; }
        public DateTime? 前年日 { get; set; }
        public string 前年曜日コード { get; set; }
        public string 前年祝日区分 { get; set; }
        public string 前年年度 { get; set; }
        public string 前年月度 { get; set; }
        public string 前年週度 { get; set; }
        public DateTime? 前年同曜日 { get; set; }
        public string Ｓａｌｅ名称１ { get; set; }
        public string Ｓａｌｅ名称２ { get; set; }
        public string Ｓａｌｅ名称３ { get; set; }
        public decimal? 月度内何週目 { get; set; }
        public string 祝日名称 { get; set; }
        public int? 週度用年度 { get; set; }
        public string 週度用会計期 { get; set; }
        public string 前年週度用年度 { get; set; }
        public string 前年同週 { get; set; }
        public DateTime? 更新日 { get; set; }
        public string 更新者 { get; set; }
        public string 削除区分 { get; set; }
        public string 平日休日区分 { get; set; }
        public string 週度期間 { get; set; }
    }
}
