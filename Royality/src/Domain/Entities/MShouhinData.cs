﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class MShouhinData
    {
        public string 服種コード { get; set; }
        public string 部門コード { get; set; }
        public string クラスコード { get; set; }
        public string 商品コード { get; set; }
        public string サイズ { get; set; }
        public string カラー { get; set; }
        public string Ｓｋｕコード { get; set; }
        public string 商品テキスト { get; set; }
        public string シーズンコード { get; set; }
        public string シーズン年度 { get; set; }
        public string ブランドコード { get; set; }
        public string メーカー品番 { get; set; }
        public long? 消し売単価 { get; set; }
        public DateTime? 有効開始日 { get; set; }
        public DateTime? 販売開始日 { get; set; }
        public DateTime? 販売終了日 { get; set; }
        public string 仕入先コード { get; set; }
        public string ＮｂＰｂ区分 { get; set; }
        public string アーカイブフラグ { get; set; }
        public string 削除フラグ { get; set; }
        public DateTime? 更新日 { get; set; }
        public DateTime? 引き取り期限日 { get; set; }
    }
}
