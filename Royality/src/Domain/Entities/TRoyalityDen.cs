﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class TRoyalityDen
    {
        public string 伝票番号 { get; set; }
        public DateTime 伝票日付 { get; set; }
        public byte 伝票タイプ { get; set; }
        public string 支払先コード { get; set; }
        public byte? 丸め単位 { get; set; }
        public byte? 丸め処理区分 { get; set; }
        public byte 税率 { get; set; }
        public int? 小計 { get; set; }
        public int? 消費税 { get; set; }
        public int 合計 { get; set; }
        public string 備考 { get; set; }
        public byte ステータス { get; set; }
        public bool? 削除フラグ { get; set; }
        public string 更新ユーザー { get; set; }
        public DateTime? 更新日時 { get; set; }
    }
}
