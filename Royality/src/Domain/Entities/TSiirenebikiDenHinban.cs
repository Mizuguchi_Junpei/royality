﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class TSiirenebikiDenHinban
    {
        public string DenpyouNo { get; set; }
        public string 自社品番 { get; set; }
        public string メーカー品番 { get; set; }
        public int 数量 { get; set; }
        public int? 単価 { get; set; }
        public int 金額 { get; set; }
    }
}
