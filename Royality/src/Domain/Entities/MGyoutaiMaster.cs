﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class MGyoutaiMaster
    {
        public string 業態コード { get; set; }
        public string 業態名 { get; set; }
        public string 業態区分 { get; set; }
    }
}
