﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class MUser
    {
        public string UserCd { get; set; }
        public string UserName { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
