﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class TRoyalityAnbun
    {
        public string 伝票番号 { get; set; }
        public string 売上伝票番号 { get; set; }
        public string 店舗コード { get; set; }
        public string 部門 { get; set; }
        public string ブランド { get; set; }
        public string 自社品番 { get; set; }
        public string メーカー品番 { get; set; }
        public string カラー { get; set; }
        public string Sku { get; set; }
        public int? 売上数量 { get; set; }
        public int? 純売上 { get; set; }
        public int? 部門別純売上計 { get; set; }
        public decimal? 部門別シェア率 { get; set; }
        public int? 部門別支払額 { get; set; }
        public int? 按分額 { get; set; }
        public int? 按分残 { get; set; }
        public int? 調整額 { get; set; }
        public int? 調整後按分額 { get; set; }
        public int? 調整後按分残 { get; set; }
    }
}
