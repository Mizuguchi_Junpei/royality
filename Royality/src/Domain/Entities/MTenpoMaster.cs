﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class MTenpoMaster
    {
        public string 店舗コード { get; set; }
        public string 事業部コード { get; set; }
        public string 事業部名 { get; set; }
        public string ブロックコード { get; set; }
        public string ブロック名 { get; set; }
        public string エリアコード { get; set; }
        public string エリア名 { get; set; }
        public string 形態区分コード { get; set; }
        public string 形態区分 { get; set; }
        public string 経年区分コード { get; set; }
        public string 経年区分 { get; set; }
        public string 契約区分コード { get; set; }
        public string 契約区分 { get; set; }
        public string 都道府県コード { get; set; }
        public string 都道府県 { get; set; }
        public string 店舗名称 { get; set; }
        public string 店舗略式名称 { get; set; }
        public string 郵便番号 { get; set; }
        public string 住所１ { get; set; }
        public string 住所２ { get; set; }
        public string 電話番号 { get; set; }
        public DateTime? プレオープン日 { get; set; }
        public DateTime? オープン日 { get; set; }
        public DateTime? 閉店日 { get; set; }
        public DateTime? 搬入開始日 { get; set; }
        public string 削除フラグ { get; set; }
        public DateTime? 有効開始日 { get; set; }
        public DateTime 更新日 { get; set; }
    }
}
