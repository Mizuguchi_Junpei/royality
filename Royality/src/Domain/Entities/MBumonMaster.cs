﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class MBumonMaster
    {
        public string 業態コード { get; set; }
        public string 業態名 { get; set; }
        public string 服種コード { get; set; }
        public string 服種名 { get; set; }
        public string 部門コード { get; set; }
        public string 部門名 { get; set; }
        public string クラスコード { get; set; }
        public string クラス名 { get; set; }
        public DateTime? 更新日 { get; set; }
    }
}
