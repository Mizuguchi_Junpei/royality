﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class TRoyalityBrand
    {
        public string 支払先コード { get; set; }
        public string ブランドコード { get; set; }
        public byte 料率 { get; set; }
    }
}
