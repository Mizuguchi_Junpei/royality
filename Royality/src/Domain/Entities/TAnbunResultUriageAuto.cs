﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class TAnbunResultUriageAuto
    {
        public string 仕入値引伝票番号 { get; set; }
        public string 自社品番 { get; set; }
        public string 店舗コード { get; set; }
        public string Skuコード { get; set; }
        public long? 売上金額 { get; set; }
        public long? 品番金額計 { get; set; }
        public int? 値引金額 { get; set; }
        public decimal? 按分率 { get; set; }
        public decimal? 値引按分金額 { get; set; }
        public decimal? 値引合計金額 { get; set; }
        public decimal? 按分差額 { get; set; }
        public long? 金額順 { get; set; }
        public int 調整額 { get; set; }
        public decimal? 調整後按分額 { get; set; }
        public decimal? 調整後按分残 { get; set; }
        public int? 売上数 { get; set; }
    }
}
