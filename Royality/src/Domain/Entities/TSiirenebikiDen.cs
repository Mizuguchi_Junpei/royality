﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class TSiirenebikiDen
    {
        public string 伝票番号 { get; set; }
        public DateTime 伝票日付 { get; set; }
        public byte 伝票タイプ { get; set; }
        public string 仕入先コード { get; set; }
        public string 業態コード { get; set; }
        public string 店舗コード { get; set; }
        public byte 税率 { get; set; }
        public int? 小計 { get; set; }
        public int? 消費税 { get; set; }
        public int? 合計 { get; set; }
        public string 理由コード { get; set; }
        public string 備考 { get; set; }
        public byte ステータス { get; set; }
        public bool? 削除flg { get; set; }
        public string 更新ユーザー { get; set; }
        public DateTime 更新日時 { get; set; }
    }
}
