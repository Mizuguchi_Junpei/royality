﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class TSiirenebikiDenMeisai
    {
        public int 順番番号 { get; set; }
        public string 伝票番号 { get; set; }
        public string 部門コード { get; set; }
        public string クラスコード { get; set; }
        public string 品名 { get; set; }
        public int 金額 { get; set; }
    }
}
