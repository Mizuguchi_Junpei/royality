﻿using MediatR;
using Microsoft.AspNetCore.Mvc.Rendering;
using Royality.Domain.Common;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Royality.Application.EnumItems.Queries.GetStatusTypeList
{
    public class GetStatusTypeListQuery : IRequest<List<SelectListItem>>
    {
    }

    public class GetDenpyoTypeListQueryHandler : IRequestHandler<GetStatusTypeListQuery, List<SelectListItem>>
    {
        public async Task<List<SelectListItem>> Handle(GetStatusTypeListQuery request, CancellationToken cancellationToken)
        {
            List<SelectListItem> lstDenTypes = Helpers.StatusValuePair.Select(S => new SelectListItem()
            {
                Value = ((int)S.Key).ToString(),
                Text = S.Value
            }).ToList();

            return await Task.FromResult(lstDenTypes);
        }
    }
}
