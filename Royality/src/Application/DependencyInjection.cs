﻿using AutoMapper;
using Domain.Entities;
using FluentValidation;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Royality.Application
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddApplication(this IServiceCollection services, IConfiguration configuration)
        {
            //int dbTimeoutInSec = int.Parse(configuration.GetSection("Settings")["DataBaseTimeoutSec"]);
            //services.AddDbContext<RoyalityContext>(options =>
            //                        options.UseSqlServer(configuration.GetConnectionString("MiniMDDBConnection"),
            //                        sqlServerOptions => sqlServerOptions.CommandTimeout(dbTimeoutInSec)));
            services.AddDbContext<RoyalityContext>(options =>
            options.UseSqlServer(configuration.GetConnectionString("MiniMDDBConnection")));


            services.AddAutoMapper(Assembly.GetExecutingAssembly());
            services.AddValidatorsFromAssembly(Assembly.GetExecutingAssembly());
            services.AddMediatR(Assembly.GetExecutingAssembly());
            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(PerformanceBehaviour<,>));
            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(ValidationBehavior<,>));
            //services.AddTransient(typeof(IPipelineBehavior<,>), typeof(UnhandledExceptionBehaviour<,>));

            services.AddScoped(typeof(RoyalityContext), typeof(RoyalityContext));

            return services;
        }
    }
}
