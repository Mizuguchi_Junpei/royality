﻿using AutoMapper;
using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Royality.Application.DenpyoItems.Commands.SaveDenpyo
{
    /// <summary>
    /// Save Denpyo Command
    /// </summary>
    public class SaveDenpyoCommand : IRequest<string>
    {
        public SaveDenpyoInfo DenpyoInfo { get; set; }
    }

    public class SaveDenpyoCommandHandler : IRequestHandler<SaveDenpyoCommand, string>
    {
        #region Private Member
        private readonly RoyalityContext _dbContext;
        private readonly IMapper _mapper;
        #endregion


        public SaveDenpyoCommandHandler(RoyalityContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public Task<string> Handle(SaveDenpyoCommand request, CancellationToken cancellationToken)
        {

            using (var transaction = _dbContext.Database.BeginTransaction(System.Data.IsolationLevel.ReadUncommitted))
            {
                try
                {
                    ////部門テーブルのデータを取得
                    ////部門伝票を取得
                    ///新しくない伝票で処理を分ける
                    
                    ///既存の伝票だったら設定を変える（あとから実装）
                    
                    ///伝票を追加して更新（bool）
                    ///更新できたら按分設定を書き換えて データリセット uriageInfoCommand, AnbunResultCommand でデータを削除
                    ///
                    ///古い伝票番号を削除
                    ///新規伝票番号を追加
                    ///トランザクションコミット
                    ///return 伝票番号をリターン


                }
                catch (Exception e)
                {
                    throw new System.NotImplementedException();

                }
            }

            throw new System.NotImplementedException();
        }
    }


}
