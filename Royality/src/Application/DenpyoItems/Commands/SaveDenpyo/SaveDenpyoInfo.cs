﻿namespace Royality.Application.DenpyoItems.Commands.SaveDenpyo
{
    public class SaveDenpyoInfo
    {
        public bool IsNewDenpyo { get; set; }
        public string SiharaiSakiCode { get; set; }
        public string DenHizuke { get; set; }
        public string DenNo { get; set; }
        public byte? DenTax { get; set; }
        public int Syoukei { get; set; }
        public int Syohizei { get; set; }
        public int Goukei { get; set; }
    }
}
