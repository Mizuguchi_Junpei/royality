﻿using AutoMapper;
using Royality.Application.Common.Mappings;
using Royality.Domain.Common;
using Royality.Domain.Enums;

namespace Royality.Application.DenpyoItems.Queries.GetDenpyoInfoList
{
    public class DenpyoInfo : IMapFrom<DenpyoInfoDto>
    {
        #region Property
        public string DenNo { get; set; }
        public string DenHiduke { get; set; }
        public string SiharaiSaki { get; set; }
        public string SiharaiSakiMei { get; set; }
        //public string Marume { get; set; }
        //public byte? Marumesyori { get; set; }
        //public string Tani { get; set; }
        public string Syokei { get; set; }
        public string Syohizei { get; set; }
        public string Goukei { get; set; }
        public string Kosinsya { get; set; }
        public string KoshinHi { get; set; }
        public string Status { get; set; }
        public bool IsCongirmed { get; set; }
        public string ResultStatus { get; set; }
        #endregion

        #region Public Member
        public void Mapping(Profile profile)
        {
            profile.CreateMap<DenpyoInfoDto, DenpyoInfo>()
            .ForMember(d => d.DenNo, opt => opt.MapFrom(S => S.DenNo))
            .ForMember(d => d.DenHiduke, opt => opt.MapFrom(S => Utilities.ConvertToDateTime(S.DenHiduke)))
            .ForMember(d => d.SiharaiSaki, opt => opt.MapFrom(S => S.SiharaiSaki))
            .ForMember(d => d.SiharaiSakiMei, opt => opt.MapFrom(S => S.SiharaiSakiMei))
            //.ForMember(d => d.Marume, opt => opt.MapFrom(S => S.Marume))
            //.ForMember(d => d.Marumesyori, opt => opt.MapFrom(S => S.Marumesyori))
            //.ForMember(d => d.Tani, opt => opt.MapFrom(S => S.Tani == null ? 0 : (int)S.Tani))
            .ForMember(d => d.Syokei, opt => opt.MapFrom(S => S.Syokei == null ? 0 : (int)S.Syokei))
            .ForMember(d => d.Syohizei, opt => opt.MapFrom(S => S.Syohizei == null ? 0 : (int)S.Syohizei))
            .ForMember(d => d.Goukei, opt => opt.MapFrom(S => S.Goukei == null ? 0 : (int)S.Goukei))
            .ForMember(d => d.Kosinsya, opt => opt.MapFrom(S => S.Kosinsya))
            .ForMember(d => d.KoshinHi, opt => opt.MapFrom(S => Utilities.ConvertToDateTime(S.KoshinHi)))
            .ForMember(d => d.Status, opt => opt.MapFrom(S => Helpers.StatusValuePair[(StatusEnum)S.Status]))
            .ForMember(d => d.IsCongirmed, opt => opt.MapFrom(S => ((StatusEnum)S.Status) == StatusEnum.Confirm));
            //.ForMember(d => d.ResultStatus, opt => opt.MapFrom(S => Helpers.ResultValuePair[S.ResultStatus]));
        }
        #endregion
    }
}