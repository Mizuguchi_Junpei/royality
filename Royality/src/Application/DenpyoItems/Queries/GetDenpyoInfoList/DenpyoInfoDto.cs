﻿using Royality.Domain.Enums;
using System;

namespace Royality.Application.DenpyoItems.Queries.GetDenpyoInfoList
{
    public class DenpyoInfoDto
    {
        #region Property
        public string DenNo { get; set; }
        public DateTime DenHiduke { get; set; }
        public string SiharaiSaki { get; set; }
        public string SiharaiSakiMei { get; set; }
        //public string Marume { get; set; }
        //public byte? Marumesyori { get; set; }
        //public int? Tani { get; set; }
        public int? Syokei { get; set; }
        public int? Syohizei { get; set; }
        public int? Goukei { get; set; }
        public string Kosinsya { get; set; }
        public DateTime KoshinHi { get; set; }
        public byte Status { get; set; }
        public ResultEnum ResultStatus { get; set; }
        #endregion
    }
}
