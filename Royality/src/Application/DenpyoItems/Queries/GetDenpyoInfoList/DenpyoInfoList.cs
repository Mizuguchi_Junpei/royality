﻿using System.Collections.Generic;

namespace Royality.Application.DenpyoItems.Queries.GetDenpyoInfoList
{
    public class DenpyoInfoList
    {
        public List<DenpyoInfo> DenpyoList { get; set; }
        public int Draw { get; set; }
        public int RecordsTotal { get; set; }
        public int RecordsFiltered { get; set; }
    }
}
