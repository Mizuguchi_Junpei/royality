﻿using AutoMapper;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Royality.Application.DenpyoItems.Queries.GetDenpyoInfoList
{
    /// <summary>
    /// Get Denoyo Info Query
    /// </summary>
    public class GetDenpyoInfoListQuery : IRequest<DenpyoInfoList>
    {
        public string DenStartDate { get; set; }
        public string DenEndDate { get; set; }
        public int StatusId { get; set; }
        public string SiharaiSakiCode { get; set; }

        public int Draw { get; set; }
        public int Start { get; set; }
        public int Length { get; set; }
    }

    public class GetDenpyoInfoListQueryHandler : IRequestHandler<GetDenpyoInfoListQuery, DenpyoInfoList>
    {
        #region Private Member
        private readonly RoyalityContext _dbContext;
        private readonly IMapper _mapper;
        #endregion

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="dbcontext"></param>
        /// <param name="mapper"></param>
        #region Constructor
        public GetDenpyoInfoListQueryHandler(RoyalityContext dbcontext, IMapper mapper)
        {
            _dbContext = dbcontext;
            _mapper = mapper;
        }
        #endregion

        #region Public Menber
        /// <summary>
        /// Query Handler
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async Task<DenpyoInfoList> Handle(GetDenpyoInfoListQuery request, CancellationToken cancellationToken)
        {
            DenpyoInfoList objDenInfoList = new DenpyoInfoList();
            DenpyoInfoQueryDto objDenpyoInfoQueryDto = _mapper.Map<DenpyoInfoQueryDto>(request);
            IQueryable<TRoyalityDen> royalityDens = _dbContext.TRoyalityDen.Where(W => W.削除フラグ != true);

            ///検索条件でフィルタリング
            if (objDenpyoInfoQueryDto != null)
            {
                royalityDens = FilterSearchCondition(objDenpyoInfoQueryDto, royalityDens);
            }

            int nRecordCnt = royalityDens.Count();

            List<DenpyoInfoDto> denpyoInfoDtos = royalityDens
                                .Join(_dbContext.TRoyalityDen,
                                    den => den.支払先コード,
                                    royality => royality.支払先コード,
                                    (den, royality) => new { den, royality })
                                .Join(_dbContext.MSiiresakiMaster,
                                den => den.royality.支払先コード,
                                siiresaki => siiresaki.仕入先コード,
                                (den, siiresaki) => new { den, siiresaki })
                                .Select(S => new DenpyoInfoDto()
                                {
                                    DenNo = S.den.royality.伝票番号,
                                    DenHiduke = S.den.royality.伝票日付,
                                    SiharaiSaki = S.den.royality.支払先コード,
                                    SiharaiSakiMei = S.siiresaki.仕入先名,
                                    //Marumesyori = S.den.royality.丸め処理区分,
                                    //Tani = S.den.royality.丸め単位,
                                    Syokei = S.den.royality.小計,
                                    Syohizei = S.den.royality.消費税,
                                    Goukei = S.den.royality.合計,
                                    Kosinsya = S.den.royality.更新ユーザー,
                                    KoshinHi = (DateTime)S.den.royality.更新日時,
                                    Status = S.den.royality.ステータス
                                })
                                .OrderBy(O => O.DenNo)
                                //.Skip(request.Start)
                                //.Take(request.Length)
                                .ToList();

            objDenInfoList.DenpyoList = denpyoInfoDtos.Select(S =>
                                        _mapper.Map<DenpyoInfo>(S)).ToList();

            objDenInfoList.Draw = request.Draw;
            objDenInfoList.RecordsTotal = nRecordCnt;
            objDenInfoList.RecordsFiltered = nRecordCnt;
            return await Task.FromResult(objDenInfoList);
        }
        #endregion

        #region Pribate Member
        /// <summary>
        /// 検索条件で伝票をフィルタリング
        /// </summary>
        /// <param name="request"></param>
        /// <param name="royalityDens"></param>
        /// <returns></returns>
        private IQueryable<TRoyalityDen> FilterSearchCondition(DenpyoInfoQueryDto request, IQueryable<TRoyalityDen> royalityDens)
        {
            ///日付のフィルタ
            ///処理状況フィルター
            ///仕入先フィルター

            if (request.DenStartDate != null)
            {
                royalityDens = royalityDens.Where(W => W.伝票日付 >= request.DenStartDate);
            }

            if (request.DenEndDate != null)
            {
                royalityDens = royalityDens.Where(W => W.伝票日付 <= request.DenEndDate);
            }

            if (request.StatusId != 0)
            {
                royalityDens = royalityDens.Where(W => W.ステータス == request.StatusId);
            }

            if (request.SiharaiSakiCode != null)
            {
                royalityDens = royalityDens.Where(W => W.支払先コード == request.SiharaiSakiCode);
            }

            return royalityDens;
        }
        #endregion
    }
}
