﻿using AutoMapper;
using Royality.Application.Common.Mappings;
using Royality.Domain.Common;
using System;

namespace Royality.Application.DenpyoItems.Queries.GetDenpyoInfoList
{
    public class DenpyoInfoQueryDto : IMapFrom<GetDenpyoInfoListQuery>
    {
        #region Property
        public DateTime? DenStartDate { get; set; }
        public DateTime? DenEndDate { get; set; }
        public byte StatusId { get; set; }
        public string SiharaiSakiCode { get; set; }
        #endregion

        #region Public Member
        /// <summary>
        /// Mapping
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<GetDenpyoInfoListQuery, DenpyoInfoQueryDto>()
                .ForMember(d => d.DenStartDate, opt => opt.MapFrom(s => Utilities.ConvertToDateTimeNullable(s.DenStartDate)))
                .ForMember(d => d.DenEndDate, opt => opt.MapFrom(s => Utilities.ConvertToDateTimeNullable(s.DenEndDate)));
        }
        #endregion
    }
}
