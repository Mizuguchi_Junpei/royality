﻿namespace Royality.Application.DenpyoItems.Queries.GetDenpyoInfo
{
    /// <summary>
    /// DenpyoDetailInfoDto
    /// </summary>
    public class DenpyoDetailInfoDto
    {
        public int RowId { get; set; }
        public string BumonName { get; set; }
        public int? Kingaku { get; set; }
    }
}
