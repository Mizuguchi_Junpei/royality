﻿using AutoMapper;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Royality.Application.DenpyoItems.Queries.GetDenpyoInfo
{
    public class GetDenpyoInfoQuery : IRequest<DenpyoInfo>
    {
        public string DenNo { get; set; }
    }

    public class GetDenpyoInfoQueryHandler : IRequestHandler<GetDenpyoInfoQuery, DenpyoInfo>
    {
        #region Private Member
        private readonly RoyalityContext _dbContext;
        private readonly IMapper _mapper;
        #endregion

        #region Constructor
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="mapper"></param>
        public GetDenpyoInfoQueryHandler(RoyalityContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }
        #endregion

        public async Task<DenpyoInfo> Handle(GetDenpyoInfoQuery request, CancellationToken cancellationToken)
        {
            DenpyoInfoDto denpyoInfoDto = new DenpyoInfoDto();

            DenpyoInfo objDenpyoInfo = _mapper.Map<DenpyoInfo>(denpyoInfoDto);

            return await Task.FromResult(objDenpyoInfo);
        }
    }
}
