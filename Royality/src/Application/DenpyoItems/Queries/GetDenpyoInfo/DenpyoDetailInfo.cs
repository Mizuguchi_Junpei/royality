﻿using AutoMapper;
using Royality.Application.Common.Mappings;

namespace Royality.Application.DenpyoItems.Queries.GetDenpyoInfo
{
    public class DenpyoDetailInfo : IMapFrom<DenpyoDetailInfoDto>
    {
        public int RowId { get; set; }
        public string BumonName { get; set; }
        public int? Kingaku { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<DenpyoDetailInfoDto, DenpyoDetailInfo>()
                   .ForMember(d => d.BumonName, opt => opt.MapFrom(s => s.BumonName))
                   .ForMember(d => d.Kingaku, opt => opt.MapFrom(s => s.Kingaku));
        }
    }
}
