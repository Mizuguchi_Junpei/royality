﻿using Royality.Domain.Common;
using System;
using System.Collections.Generic;

namespace Royality.Application.DenpyoItems.Queries.GetDenpyoInfo
{
    public class DenpyoInfoDto
    {
        public string SiharaiSakiCode { get; set; }
        public string SiharaiSakiName { get; set; }
        public DateTime? DenHiduke { get; set; }
        public string DenNo { get; set; }
        public string Remarks { get; set; }
        public byte DenTax { get; set; }
        public int? Syukei { get; set; }
        public int? Shohizei { get; set; }
        public int? Goukei { get; set; }
        public DateTime? KousinHi { get; set; }
        public List<DenpyoDetailInfo> DenpyoDetailList { get; set; }

        public DenpyoInfoDto()
        {
            DenTax = Constants.DEFAULT_DEN_TAX;
        }
    }
}
