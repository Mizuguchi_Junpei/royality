﻿using Royality.Application.Common.Mappings;
using Royality.Domain.Common;
using System.Collections.Generic;

namespace Royality.Application.DenpyoItems.Queries.GetDenpyoInfo
{
    public class DenpyoInfo : IMapFrom<DenpyoInfoDto>
    {
        public string SiharaiSakiCode { get; set; }
        public string SiharaiSakiName { get; set; }
        public string DenHiduke { get; set; }
        public string DenNo { get; set; }
        public string Remarks { get; set; }
        public string DenTax { get; set; }
        public int Syukei { get; set; }
        public int Shohizei { get; set; }
        public int Goukei { get; set; }
        public List<DenpyoDetailInfo> DenpyoDetailList { get; set; }

        public void Mapping(MappingProfile profile)
        {
            profile.CreateMap<DenpyoInfoDto, DenpyoInfo>()
                .ForMember(d => d.DenNo, opt => opt.MapFrom(s => s.DenNo))
                .ForMember(d => d.DenHiduke, opt => opt.MapFrom(s => Utilities.ConvertToDateTime(s.DenHiduke)))
                .ForMember(d => d.SiharaiSakiCode, opt => opt.MapFrom(s => s.SiharaiSakiCode))
                .ForMember(d => d.SiharaiSakiName, opt => opt.MapFrom(s => s.SiharaiSakiName))
                .ForMember(d => d.Shohizei, opt => opt.MapFrom(s => s.Shohizei == null ? 0 : (int)s.Shohizei))
                .ForMember(d => d.Syukei, opt => opt.MapFrom(s => s.Syukei == null ? 0 : (int)s.Syukei))
                .ForMember(d => d.Goukei, opt => opt.MapFrom(s => s.Goukei == null ? 0 : (int)s.Goukei))
                .ForMember(d => d.Remarks, opt => opt.MapFrom(s => s.Remarks))
                .ForMember(d => d.DenpyoDetailList, opt => opt.MapFrom(s => s.DenpyoDetailList));
        }
    }
}
