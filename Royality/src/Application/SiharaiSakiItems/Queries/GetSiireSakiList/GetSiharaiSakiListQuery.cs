﻿using Domain.Entities;
using MediatR;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Royality.Application.SiharaiSakiItems.Queries.GetSiireSakiList
{
    public class GetSiharaiSakiListQuery : IRequest<List<Siharaisaki>>
    {
    }

    public class GetSiharaiSakiListQueryHandler : IRequestHandler<GetSiharaiSakiListQuery, List<Siharaisaki>>
    {
        #region Private Member
        private readonly RoyalityContext _dbContext;
        #endregion

        #region Constructor
        public GetSiharaiSakiListQueryHandler(RoyalityContext dbContext)
        {
            _dbContext = dbContext;
        }
        #endregion

        #region Private Member
        public async Task<List<Siharaisaki>> Handle(GetSiharaiSakiListQuery request, CancellationToken cancellationToken)
        {
            List<Siharaisaki> lstSiharaiSakiInfo = new List<Siharaisaki>();
            lstSiharaiSakiInfo = _dbContext.TRoyalityPayee.Select(S => new Siharaisaki()
            {
                Code = S.支払先コード,
                Name = S.支払先名
            }).Distinct().ToList();

            return await Task.FromResult(lstSiharaiSakiInfo);
        }
        #endregion
    }
}
