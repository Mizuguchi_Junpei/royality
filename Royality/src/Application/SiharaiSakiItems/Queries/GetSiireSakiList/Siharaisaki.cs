﻿namespace Royality.Application.SiharaiSakiItems.Queries
{
    public class Siharaisaki
    {
        public string Code { get; set; }
        public string Name { get; set; }
    }
}
