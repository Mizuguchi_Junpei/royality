﻿using System.Collections.Generic;

namespace Royality.Application.Common.Models
{
    public class AnbunResult
    {
        public List<AnbunResultUriage> anbunResultListUriage { get; set; }

        public List<AnbunResultBummon> anbunResultListBummon { get; set; }
    }
}
