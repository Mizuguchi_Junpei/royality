﻿namespace Royality.Application.Common.Models
{
    public class AnbunSetting
    {
        public string DenNo { get; set; }
        public int? AnbunMethod { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
    }
}
