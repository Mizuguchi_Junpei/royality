﻿using AutoMapper;
using Royality.Application.Common.Mappings;
using Royality.Domain.Common;
using Royality.Domain.Enums;
using System;

namespace Royality.Application.Common.Models
{
    public class AnbunSettingDTO : IMapFrom<AnbunSetting> 
    {
        public string DenNo { get; set; }
        public AnbunMethodEnum AnbunMethod { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }

        public void Mapping(Profile profile)
        {
            profile.CreateMap<AnbunSetting, AnbunSettingDTO>()
                .ForMember(d => d.StartDate, opt => opt.MapFrom(S => Utilities.ConvertToDateTimeNullable(S.StartDate)))
                .ForMember(d => d.EndDate, opt => opt.MapFrom(S => Utilities.ConvertToDateTimeNullable(S.EndDate)))
                .ForMember(d => d.AnbunMethod, opt => opt.MapFrom(S => S.AnbunMethod));
        }
    }
}
