﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Royality.Application.Common.Models
{
    public class AnbunResultUriage
    {
        public string TenpoCode { get; set; }
        public string TenpoName { get; set; }
        public decimal AnbunAmt { get; set; }
        public decimal AnbunRitsu { get; set; }
    }
}
