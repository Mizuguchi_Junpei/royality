﻿namespace Royality.Application.Common.Models
{
    public class AnbunResultUriageDTO
    {
        public string TenpoCode { get; set; }
        public string SkuCode { get; set; }
        public int Qty { get; set; }
        public int UnitPrice { get; set; }
        public int Amt { get; set; }
        public decimal Ritsu{ get; set; }
    }
}
