﻿namespace Royality.Application.Common.Models
{
    public class UriageInfo
    {
        public string RoHinban { get; set; }
        public string MakerHinban { get; set; }
        public int? UnitPrice { get; set; }
        public int? Unit { get; set; }
        public int? Amount { get; set; }
    }
}
