﻿using AutoMapper;

namespace Royality.Application.Common.Mappings
{
    /// <summary>
    /// Interface for object mapping 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IMapFrom<T>
    {
        void Mapping(Profile profile) => profile.CreateMap(typeof(T), GetType());
    }
}
