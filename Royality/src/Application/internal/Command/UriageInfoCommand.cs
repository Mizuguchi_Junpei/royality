﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Royality.Application.Internal.Command.UriageInfo
{
    internal class UriageInfoCommand
    {
        #region internal Member
        /// <summary>
        /// Save Uriage Command Method
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="denNo"></param>
        /// <param name="uriageInfoList"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        internal static async Task SaveAsync(RoyalityContext dbContext, string denNo, List<Common.Models.UriageInfo> uriageInfoList, CancellationToken cancellationToken)
        {
            try
            {
                if (uriageInfoList.Count > 0)
                {
                    List<TRoyalityDenUriage> lstRoyalityDen = new List<TRoyalityDenUriage>();

                    foreach (Common.Models.UriageInfo uriageInfo in uriageInfoList)
                    {
                        TRoyalityDenUriage royalityDenUriage = new TRoyalityDenUriage();
                        royalityDenUriage.DenpyoNo = denNo;
                        royalityDenUriage.自社品番 = uriageInfo.RoHinban;
                        royalityDenUriage.メーカー品番 = uriageInfo.MakerHinban;
                        royalityDenUriage.数量 = (int)uriageInfo.Unit;
                        royalityDenUriage.単価 = (int)uriageInfo.UnitPrice;
                        royalityDenUriage.金額 = (int)uriageInfo.Amount;

                        lstRoyalityDen.Add(royalityDenUriage);
                    }

                    await dbContext.TRoyalityDenUriage.AddRangeAsync(lstRoyalityDen, cancellationToken);
                    await dbContext.SaveChangesAsync(cancellationToken);
                }
            }
            catch (Exception e)
            {
            }
        }

        /// <summary>
        /// Delete Uriage Info
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="denNo"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        internal static async Task DeleteAsync(RoyalityContext dbContext, string denNo, CancellationToken cancellationToken)
        {
            List<TRoyalityDenUriage> lstDenUriage = dbContext.TRoyalityDenUriage.Where(W => W.DenpyoNo == denNo).ToList();
            if (lstDenUriage.Count > 0)
            {
                dbContext.RemoveRange(lstDenUriage);
                await dbContext.SaveChangesAsync(cancellationToken);
            }
        }
        #endregion
    }
}
