﻿using Domain.Entities;
using EFCore.BulkExtensions;
using Royality.Application.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Royality.Application.Internal.Command.AnbunResultCommand
{
    internal class AnbunResultCommand
    {
        #region
        /// <summary>
        /// Save Auto Anbun Command Method
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="anbunResultList"></param>
        /// <param name="cancelltionToken"></param>
        /// <returns></returns>
        internal static async Task AutoAnbunResultSaveAsync(RoyalityContext dbContext, List<TRoyalityAnbun> anbunResultList, CancellationToken cancellationToken)
        {
            try
            {
                if (anbunResultList.Count() > 0)
                {
                    BulkConfig bulkConfig = new BulkConfig();
                    await dbContext.BulkInsertAsync(anbunResultList, bulkConfig, null, cancellationToken);
                    //await dbContext.TAnbunResultUriageAuto.AddRangeAsync(anbunResultList, cancellationToken);
                    //await dbContext.SaveChangesAsync(cancellationToken);
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

        /// <summary>
        /// Delete Auto Anbun result
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="denNo"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        internal static async Task AutoAnbunResultDeleteAsync(RoyalityContext dbContext, string denNo, CancellationToken cancellationToken)
        {
            try
            {
                List<TRoyalityAnbun> lstResultDelete = dbContext.TRoyalityAnbun
                                                              .Where(W => W.伝票番号 == denNo).ToList();
                if (lstResultDelete.Count() > 0)
                {
                    BulkConfig bulkConfig = new BulkConfig();
                    await dbContext.BulkDeleteAsync(lstResultDelete, bulkConfig, null, cancellationToken);
                    //dbContext.RemoveRange(lstResultDelete);
                    //await dbContext.SaveChangesAsync(cancellationToken);
                }
            }
            catch (Exception e)
            {

            }
        }

        /// <summary>
        /// Save Anbun Command Method
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="denNo"></param>
        /// <param name="anbunResultList"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        internal static async Task SaveAsync(RoyalityContext dbContext, string denNo, List<AnbunResultUriageDTO> anbunResultList, CancellationToken cancellationToken)
        {
            if (anbunResultList.Count() > 0)
            {
                BulkConfig bulkConfig = new BulkConfig();
                List<TAnbunResultUriage> lstResult = anbunResultList.Select(S => new TAnbunResultUriage()
                {
                    DenpyouNo = denNo,
                    TenpoCode = S.TenpoCode,
                    Sku = S.SkuCode,
                    Qty = S.Qty,
                    Amt = S.Amt,
                    Ritsu = S.Ritsu
                }).ToList();

                await dbContext.BulkInsertAsync(lstResult, bulkConfig, null, cancellationToken);
            }
        }

        /// <summary>
        /// Delete Anbun result
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="denNo"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        internal static async Task DeleteAsync(RoyalityContext dbContext, string denNo, CancellationToken cancellationToken)
        {
            List<TAnbunResultUriage> listDelete = dbContext.TAnbunResultUriage.Where(W => W.DenpyouNo == denNo).ToList();
            try
            {
                if (listDelete.Count() > 0)
                {
                    BulkConfig bulkConfig = new BulkConfig();
                    await dbContext.BulkDeleteAsync(listDelete, bulkConfig, null, cancellationToken);
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }
        #endregion
    }
}