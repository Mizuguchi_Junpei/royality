﻿namespace Royality.Application.Internal.Queries.AnbunQuery
{
    /// <summary>
    /// Anbun Query Result
    /// </summary>
    public class AnbunQueryResult
    {
        public string 店舗コード { get; set; }
        public string 自社品番 { get; set; }
        public string Skuコード { get; set; }
        public long? 金額 { get; set; }
        public int 売上金額 { get; set; }
        public int? 数 { get; set; }
    }
}
