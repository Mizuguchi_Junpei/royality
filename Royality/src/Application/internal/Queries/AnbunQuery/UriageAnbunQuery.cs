﻿using Domain.Entities;
using Royality.Application.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Royality.Application.Internal.Queries.AnbunQuery
{
    internal class UriageAnbunQuery
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dbContext"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        internal static IQueryable<AnbunQueryResult> GetQuery(RoyalityContext dbContext, AnbunSettingDTO request)
        {
            var query = dbContext.TRoyalityDenUriage
                                    .Where(W => W.DenpyoNo == request.DenNo)
                                    .Select(S => new
                                    {
                                        S.DenpyoNo,
                                        S.自社品番,
                                        S.金額,
                                    })
                                    .Join(dbContext.MShouhinData,
                                    denpyo => denpyo.自社品番,
                                    mst => mst.商品コード,
                                    (denpyo, mst) => new
                                    {
                                        denpyo,
                                        mst.Ｓｋｕコード
                                    })
                                    .Join(dbContext.DUriageDenpyouMeisai,
                                    mst => mst.Ｓｋｕコード,
                                    uri => uri.Skuコード,
                                    (mst, uri) => new { mst, uri })
                                    .Where(W => W.uri.売上伝票日付 >= request.StartDate &&
                                    W.uri.売上伝票日付 <= request.EndDate);

            var queryResultList = query.GroupBy(G => new
                                            {
                                                G.mst.denpyo.DenpyoNo,
                                                G.uri.店舗コード,
                                                G.mst.denpyo.自社品番,
                                                G.uri.Skuコード,
                                                G.mst.denpyo.金額
                                            })
                                        .Where(W => W.Sum(Sm => Sm.uri.勘定コード == "802" ? (-1 * Sm.uri.売上売価) : Sm.uri.売上売価) > 0)
                                        .Select(S => new AnbunQueryResult
                                        {
                                            店舗コード = S.Key.店舗コード,
                                            自社品番 = S.Key.自社品番,
                                            Skuコード = S.Key.Skuコード,
                                            金額 = S.Sum(Sm => Sm.uri.勘定コード == "802" ? (-1 * (Sm.uri.売上売価 + Sm.uri.売上割引)) : (Sm.uri.売上売価 + Sm.uri.売上割引)),
                                            売上金額 = S.Key.金額,
                                            数 = S.Sum(Sm => Sm.uri.売上数)
                                        });

            return queryResultList;
        }
    }
}
