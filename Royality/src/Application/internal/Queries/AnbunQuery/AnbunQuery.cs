﻿using Domain.Entities;
using Royality.Application.Common.Models;
using System.Linq;

namespace Royality.Application.Internal.Queries.AnbunQuery
{
    internal class AnbunQuery
    {
        internal static IQueryable<AnbunQueryResult> GetQuery(RoyalityContext dbContextm, AnbunSettingDTO queryInfo)
        {
            IQueryable<AnbunQueryResult> lstQuery;

            lstQuery = UriageAnbunQuery.GetQuery(dbContextm, queryInfo);

            return lstQuery;
        }
    }
}
