﻿using AutoMapper;
using Royality.Application.Common.Mappings;

namespace Royality.Application.AnbunItems.Queries.GetDenpyoInfoUriageList
{
    public class DenpyoInfoUriage : IMapFrom<DenpyoInfoUriageDto>
    {
        #region Property
        public string JisyaHinban { get; set; }
        public string MakerHinban { get; set; }
        public long? Suryo { get; set; }
        public int? Tanka { get; set; }
        public long? Goukei { get; set; }
        #endregion

        #region Public Member
        public void Mapping(Profile profile)
        {
            profile.CreateMap<DenpyoInfoUriageDto, DenpyoInfoUriage>()
                .ForMember(d => d.JisyaHinban, opt => opt.MapFrom(s => s.JisyaHinban))
                .ForMember(d => d.MakerHinban, opt => opt.MapFrom(s => s.MakerHinban))
                .ForMember(d => d.Suryo, opt => opt.MapFrom(s => s.Suryo))
                .ForMember(d => d.Tanka, opt => opt.MapFrom(s => s.Tanka))
                .ForMember(d => d.Goukei, opt => opt.MapFrom(s => s.Goukei));
        }
        #endregion
    }
}
