﻿using AutoMapper;
using Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Royality.Application.AnbunItems.Queries.GetDenpyoInfoUriageList
{
    public class GetDenpyoInfoListUriageQuery : IRequest<DenpyoInfoListUriage>
    {
        #region Property
        public string DenNo { get; set; }
        public string SiharaiSakiCode { get; set; }
        public string DenStartDate { get; set; }
        public string DenEndDate { get; set; }
        #endregion
    }

    public class GetDenpyoInfoListUriageQueryHandler : IRequestHandler<GetDenpyoInfoListUriageQuery, DenpyoInfoListUriage>
    {
        #region Private Member
        private readonly RoyalityContext _dbContext;
        private readonly IMapper _mapper;
        #endregion

        #region Constructor
        public GetDenpyoInfoListUriageQueryHandler(RoyalityContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }
        #endregion

        public async Task<DenpyoInfoListUriage> Handle(GetDenpyoInfoListUriageQuery request, CancellationToken cancellationToken)
        {
            try
            {
                DenpyoInfoListUriage objDenInfoListUriage = new DenpyoInfoListUriage();
                ///検索モデル取得　mapで挿入
                DenpyoInfoQueryUriageDto objDenpyoInfoQueryUriageDto = _mapper.Map<DenpyoInfoQueryUriageDto>(request);
                ///売上データ取得
                IQueryable<DUriageDenpyouMeisai> uriageDenpyouMeisai = _dbContext.DUriageDenpyouMeisai.Where(w => w.売上数 != 0);

                ///支払先と日付で検索　売れた日付と紐づける
                if (objDenpyoInfoQueryUriageDto != null)
                {
                    uriageDenpyouMeisai = FilterConditionUriage(objDenpyoInfoQueryUriageDto, uriageDenpyouMeisai);
                }

                string rBrand = _dbContext.TRoyalityBrand
                    .Where(W => W.支払先コード == objDenpyoInfoQueryUriageDto.SiharaiSakiCode)
                    .Select(S => S.ブランドコード).Single();

                int nRecordCnt = uriageDenpyouMeisai.Count();

                //List<DenpyoInfoUriageDto> denpyoInfoDtos = uriageDenpyouMeisai
                //                .Join(_dbContext.MShouhinData,
                //                uriage => uriage.Skuコード,
                //                shohin => shohin.Ｓｋｕコード,
                //                (uriage, shohin) => new { uriage, shohin })
                //                .GroupBy(x => new
                //                {
                //                    x.shohin.メーカー品番
                //                })
                //                .Select(S => new DenpyoInfoUriageDto()
                //                {
                //                    JisyaHinban = S.Min(y => y.shohin.商品コード),
                //                    MakerHinban = S.Key.メーカー品番,
                //                    Suryo = S.Sum(y => y.uriage.売上数),
                //                    Tanka = (int?)S.Max(y => y.uriage.売上売価),
                //                    Goukei = (int?)S.Sum(y => y.uriage.売上売価)
                //                })
                //                .OrderBy(o => o.Suryo)
                //                .ThenBy(o => o.Goukei)
                //                .ThenBy(o => o.Tanka)
                //                .ThenBy(o => o.MakerHinban)
                //                .ToList();

                List<DenpyoInfoUriageDto> denpyoInfoDtos = uriageDenpyouMeisai
                .Join(_dbContext.MShouhinData,
                uriage => uriage.Skuコード,
                shohin => shohin.Ｓｋｕコード,
                (uriage, shohin) => new { uriage, shohin })
                .Where(S => S.shohin.ブランドコード == rBrand)
                .GroupBy(x => new
                {
                    x.shohin.メーカー品番
                })
                .Select(S => new DenpyoInfoUriageDto()
                {
                    JisyaHinban = S.Min(y => y.shohin.商品コード),
                    MakerHinban = S.Key.メーカー品番,
                    Suryo = S.Sum(y => y.uriage.売上数),
                    Tanka = (int?)S.Max(y => y.uriage.売上売価),
                    Goukei = (int?)S.Sum(y => y.uriage.売上売価)
                })
                .OrderBy(o => o.Suryo)
                .ThenBy(o => o.Goukei)
                .ThenBy(o => o.Tanka)
                .ThenBy(o => o.MakerHinban)
                .ToList();

                //List<DenpyoInfoUriageDto> denpyoInfoDtos = uriageDenpyouMeisai
                //                            .Join(_dbContext.MShouhinData,
                //                            uriage => uriage.Skuコード,
                //                            shohin => shohin.Ｓｋｕコード,
                //                            (uriage, shohin) => new { uriage, shohin })
                //                            .Join(_dbContext.TRoyalityBrand,
                //                            ushohin => ushohin.shohin.
                //                            )

                //                            .Where(S => S.shohin.ブランドコード == "0001")
                //                            .GroupBy(x => new
                //                            {
                //                                x.shohin.メーカー品番
                //                            })
                //                            .Select(S => new DenpyoInfoUriageDto()
                //                            {
                //                                JisyaHinban = S.Min(y => y.shohin.商品コード),
                //                                MakerHinban = S.Key.メーカー品番,
                //                                Suryo = S.Sum(y => y.uriage.売上数),
                //                                Tanka = (int?)S.Max(y => y.uriage.売上売価),
                //                                Goukei = (int?)S.Sum(y => y.uriage.売上売価)
                //                            })
                //                            .OrderBy(o => o.Suryo)
                //                            .ThenBy(o => o.Goukei)
                //                            .ThenBy(o => o.Tanka)
                //                            .ThenBy(o => o.MakerHinban)
                //                            .ToList();

                ///リストにマッピング挿入

                objDenInfoListUriage.DenpyoList = denpyoInfoDtos.Select(S => _mapper.Map<DenpyoInfoUriage>(S)).ToList();
                ///登録されたデータ合計をカウント
                objDenInfoListUriage.ImpRecordsTotal = nRecordCnt;
                ///フィルタリング後のカウント計算
                objDenInfoListUriage.ImpRecordsFiltered = nRecordCnt;

                return await Task.FromResult(objDenInfoListUriage);

            }
            catch (Exception e)
            {
                throw;
            }
        }

        private IQueryable<DUriageDenpyouMeisai> FilterConditionUriage(DenpyoInfoQueryUriageDto request, IQueryable<DUriageDenpyouMeisai> uriageDenpyouMeisai)
        {
            if (request.DenStartDate != null)
            {
                uriageDenpyouMeisai = uriageDenpyouMeisai.Where(w => w.売上伝票日付 >= request.DenStartDate);
            }
            if (request.DenEndDate != null)
            {
                uriageDenpyouMeisai = uriageDenpyouMeisai.Where(w => w.売上伝票日付 <= request.DenEndDate);
            }

            return uriageDenpyouMeisai;
        }
    }
}
