﻿using AutoMapper;
using Royality.Application.Common.Mappings;
using Royality.Domain.Common;
using System;

namespace Royality.Application.AnbunItems.Queries.GetDenpyoInfoUriageList
{
    public class DenpyoInfoQueryUriageDto : IMapFrom<GetDenpyoInfoListUriageQuery>
    {
        #region Property
        public string DenNo { get; set; }
        public string SiharaiSakiCode { get; set; }
        public DateTime DenStartDate { get; set; }
        public DateTime DenEndDate { get; set; }
        #endregion

        #region Public Member
        /// <summary>
        /// Mapping
        /// </summary>
        /// <param name="profile"></param>
        public void Mapping(Profile profile)
        {
            profile.CreateMap<GetDenpyoInfoListUriageQuery, DenpyoInfoQueryUriageDto>()
                .ForMember(d => d.SiharaiSakiCode, opt => opt.MapFrom(s => s.SiharaiSakiCode))
                .ForMember(d => d.DenStartDate, opt => opt.MapFrom(s => Utilities.ConvertToDateTimeNullable(s.DenStartDate)))
                .ForMember(d => d.DenEndDate, opt => opt.MapFrom(s => Utilities.ConvertToDateTimeNullable(s.DenEndDate)));
        }
        #endregion
    }

}
