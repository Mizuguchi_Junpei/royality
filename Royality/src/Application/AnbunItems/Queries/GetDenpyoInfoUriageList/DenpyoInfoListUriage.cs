﻿using System.Collections.Generic;

namespace Royality.Application.AnbunItems.Queries.GetDenpyoInfoUriageList
{
    public class DenpyoInfoListUriage
    {
        public List<DenpyoInfoUriage> DenpyoList { get; set; }
        public int ImpRecordsTotal { get; set; }
        public int ImpRecordsFiltered { get; set; }
    }
}
