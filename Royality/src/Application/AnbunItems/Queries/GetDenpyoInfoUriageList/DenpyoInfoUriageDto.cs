﻿namespace Royality.Application.AnbunItems.Queries.GetDenpyoInfoUriageList
{
    public class DenpyoInfoUriageDto
    {
        #region Property
        public string JisyaHinban { get; set; }
        public string MakerHinban { get; set; }
        public int? Suryo { get; set; }
        public int? Tanka { get; set; }
        public int? Goukei { get; set; }
        #endregion
    }

}
