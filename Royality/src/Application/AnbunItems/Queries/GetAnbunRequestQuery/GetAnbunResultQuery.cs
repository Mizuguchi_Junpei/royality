﻿using Domain.Entities;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Royality.Application.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Royality.Application.AnbunItems.Queries
{
    public class GetAnbunResultQuery : IRequest<AnbunResult>
    {
        public string DenNo { get; set; }
    }

    public class GetAnbunResultQueryHandler : IRequestHandler<GetAnbunResultQuery, AnbunResult>
    {
        #region Private Member
        private readonly RoyalityContext _dbContext;
        #endregion

        #region Constructor
        public GetAnbunResultQueryHandler(RoyalityContext dbContext)
        {
            _dbContext = dbContext;
        }
        #endregion

        #region Public Member
        public async Task<AnbunResult> Handle(GetAnbunResultQuery request, CancellationToken cancellationToken)
        {
            AnbunResult anbunResult = new AnbunResult();
            anbunResult.anbunResultListUriage = await GetAnbunResultUriage(request, cancellationToken);
            anbunResult.anbunResultListBummon = await GetAnbunResultBummon(request, cancellationToken);

            return anbunResult;
        }
        #endregion


        #region Private Member
        private async Task<List<AnbunResultUriage>> GetAnbunResultUriage(GetAnbunResultQuery request, CancellationToken cancellationToken)
        {
            var queryResult = await _dbContext.TAnbunResultUriage.Where(W => W.DenpyouNo == request.DenNo)
                                    .Join(_dbContext.MTenpoMaster,
                                    result => result.TenpoCode,
                                    tenpo => tenpo.店舗コード,
                                    (result, tenpo) => new { result, tenpo })
                                    .Select(S => new { S.result.TenpoCode, S.tenpo.店舗名称, S.result.Amt, S.result.Ritsu })
                                    .GroupBy(G => new { G.TenpoCode, G.店舗名称 })
                                    .Select(S => new
                                    {
                                        S.Key.TenpoCode,
                                        S.Key.店舗名称,
                                        AnbunAmt = S.Sum(S => S.Amt),
                                        AnbunRitsu = S.Sum(S => S.Ritsu)
                                    }).ToListAsync(cancellationToken);

            decimal dTotalAmt = queryResult.Sum(S => (decimal)S.AnbunAmt);
            List<AnbunResultUriage> listAnbunResult = queryResult.Select(S => new AnbunResultUriage
            {
                TenpoCode = S.TenpoCode,
                TenpoName = S.店舗名称,
                AnbunAmt = S.AnbunAmt,
                AnbunRitsu = decimal.Round(Convert.ToDecimal(S.AnbunAmt) / dTotalAmt, 5),

            }).OrderBy(O => O.AnbunAmt).ToList();

            return listAnbunResult;
        }

        private async Task<List<AnbunResultBummon>> GetAnbunResultBummon(GetAnbunResultQuery request, CancellationToken cancellationToken)
        {
            List<AnbunResultBummon> queryResult = await _dbContext.TRoyalityAnbun.Where(W => W.伝票番号 == request.DenNo)
                .Select(S => new AnbunResultBummon
                {
                    BummonName = S.部門,
                    Kingaku = (int)S.按分額,
                })
                .OrderBy(O => O.Kingaku)
                .ToListAsync(cancellationToken);

            return queryResult;
        }
        #endregion
    }
}
