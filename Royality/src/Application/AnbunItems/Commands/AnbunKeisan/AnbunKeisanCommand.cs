﻿using AutoMapper;
using Domain.Entities;
using MediatR;
using Royality.Application.Common.Models;
using Royality.Application.Internal.Command.AnbunResultCommand;
using Royality.Application.Internal.Command.UriageInfo;
using Royality.Application.Internal.Queries.AnbunQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Royailty.Application.AnbunItems.Commands
{
    /// <summary>
    /// Anbun Keisan Command
    /// </summary>
    public class AnbunKeisanCommand : IRequest
    {
        public AnbunSetting AnbunSetting { get; set; }
        public List<UriageInfo> UriageInfoList { get; set; }
    }

    public class AnbunKeisanCommandHandler : IRequestHandler<AnbunKeisanCommand>
    {
        #region Private Member
        private readonly RoyalityContext _dbContext;
        private readonly IMapper _mapper;
        #endregion

        #region Constructor
        public AnbunKeisanCommandHandler(RoyalityContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }
        #endregion

        #region Public Member
        public async Task<Unit> Handle(AnbunKeisanCommand request, CancellationToken cancellationToken)
        {
            try
            {
                using (var transaction = _dbContext.Database.BeginTransaction())
                {
                    AnbunSettingDTO commandInfo = _mapper.Map<AnbunSettingDTO>(request.AnbunSetting);

                    await UriageInfoCommand.DeleteAsync(_dbContext, commandInfo.DenNo, cancellationToken);
                    await UriageInfoCommand.SaveAsync(_dbContext, commandInfo.DenNo, request.UriageInfoList, cancellationToken);


                    IQueryable<AnbunQueryResult> query = AnbunQuery.GetQuery(_dbContext, commandInfo);
                    List<AnbunQueryResult> queryResult = query.ToList();

                    ///ここでロイヤリティ按分して伝票を作成
                    List<TRoyalityAnbun> lstAnbunResult = CalculateAnbunQueryResult(queryResult, request.AnbunSetting.DenNo);

                    await AnbunResultCommand.AutoAnbunResultDeleteAsync(_dbContext, request.AnbunSetting.DenNo, cancellationToken);
                    await AnbunResultCommand.AutoAnbunResultSaveAsync(_dbContext, lstAnbunResult, cancellationToken);

                    var lstResult = lstAnbunResult.Select(S => new AnbunResultUriageDTO()
                    {
                        TenpoCode = S.店舗コード,
                        SkuCode = S.Sku,
                        Qty = (int)S.売上数量,
                        Amt = (int)S.調整後按分額,
                        Ritsu = (decimal)S.部門別シェア率,
                    }).ToList();

                    await AnbunResultCommand.DeleteAsync(_dbContext, request.AnbunSetting.DenNo, cancellationToken);
                    await AnbunResultCommand.SaveAsync(_dbContext, commandInfo.DenNo, lstResult, cancellationToken);

                    await transaction.CommitAsync(cancellationToken);
                    return Unit.Value;
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }
        #endregion

        #region Private Member

        /// <summary>
        /// 按分計算
        /// </summary>
        /// <param name="lstResult"></param>
        /// <param name="denNo"></param>
        /// <returns></returns>
        private List<TRoyalityAnbun> CalculateAnbunQueryResult(List<AnbunQueryResult> listResult, string denNo)
        {

            var b1 = listResult.GroupBy(G => G.自社品番)
                .Select(S => new { S, 売上金額計 = S.Sum(Sm => Sm.金額) })
                .SelectMany(Sm => Sm.S.Select(S => new
                {
                    S.自社品番,
                    S.店舗コード,
                    S.Skuコード,
                    S.金額,
                    Sm.売上金額計,
                    S.売上金額,
                    S.数,
                    按分率 = ((Convert.ToDecimal(S.金額) / Convert.ToDecimal(Sm.売上金額計))),
                    売上按分金額 = Math.Floor(S.売上金額 * ((Convert.ToDecimal(S.金額) / Convert.ToDecimal(Sm.売上金額計)))),
                })).ToList();


            var b2 = b1
                .GroupBy(G => G.自社品番)
                .Select(S => new { S, 売上合計金額 = S.Sum(Sm => Sm.売上按分金額) })
                .SelectMany(Sm => Sm.S.Select(S => new
                {
                    S.自社品番,
                    S.店舗コード,
                    S.Skuコード,
                    S.金額,
                    S.売上金額計,
                    S.売上金額,
                    S.数,
                    S.按分率,
                    S.売上按分金額,
                    Sm.売上合計金額,
                    按分差額 = S.売上金額 - Sm.売上合計金額,
                })).ToList();


            var b4 = b2.OrderBy(O => O.自社品番).ThenByDescending(O => O.金額)
                .GroupBy(G => G.自社品番).Select(S => new { S, Count = S.Count() })
                .SelectMany(SM => SM.S.Select(S => S)
                .Zip(Enumerable.Range(1, SM.Count), (row, index) =>
                 new
                 {
                     row.自社品番,
                     row.店舗コード,
                     row.Skuコード,
                     row.金額,
                     row.売上金額計,
                     row.売上金額,
                     row.数,
                     row.按分率,
                     row.売上按分金額,
                     row.売上合計金額,
                     按分差額 = row.売上金額 - row.売上合計金額,
                     金額順 = index,
                 })).ToList();


            var b5 = b4.Select(S => new
            {
                S.自社品番,
                S.店舗コード,
                S.Skuコード,
                S.金額,
                S.売上金額計,
                S.売上金額,
                S.数,
                S.按分率,
                S.売上按分金額,
                S.売上合計金額,
                S.按分差額,
                S.金額順,
                調整額 = (S.按分差額 > 0 && Math.Abs(S.按分差額) >= S.金額順 ? 1 : S.按分差額 < 0 && Math.Abs(S.按分差額) >= S.金額順 ? -1 : 0)
            }).ToList();


            List<TRoyalityAnbun> b6 = b5
                .GroupBy(G => G.自社品番)
                .Select(S => new { S, 調整後按分合計 = S.Sum(Su => (Su.売上按分金額 + Su.調整額)) })
                .SelectMany(Sm => Sm.S.Select(S => new TRoyalityAnbun
                {
                    伝票番号 = denNo,
                    自社品番 = S.自社品番,
                    店舗コード = S.店舗コード,
                    Sku = S.Skuコード,
                    純売上 = S.売上金額,
                    売上数量 = S.数,
                    按分額 = (int?)S.売上按分金額,
                    調整額 = S.調整額,
                    部門別シェア率 = (decimal?)S.按分率,
                    調整後按分額 = (int?)(S.売上按分金額 - Sm.調整後按分合計)
                })).ToList();


            List<TRoyalityAnbun> ListAnbunResult = b6
                .Join(_dbContext.MShouhinData,
                                uriage => uriage.Sku,
                                shohin => shohin.Ｓｋｕコード,
                                (uriage, shohin) => new { uriage, shohin })
                .Join(_dbContext.MBumonMaster,
                            ushohin => ushohin.shohin.部門コード,
                            bumon => bumon.部門コード,
                            (ushohin, bumon) => new { ushohin, bumon })
                .GroupBy(G => G.ushohin.uriage.自社品番)
                .Select(S => new TRoyalityAnbun
                {
                    伝票番号 = S.Min(y => y.ushohin.uriage.伝票番号),
                    店舗コード = S.Min(y => y.ushohin.uriage.店舗コード),
                    部門 = S.Min(y => y.bumon.部門名),
                    自社品番 = S.Min(y => y.ushohin.uriage.自社品番),
                    メーカー品番 = S.Min(y => y.ushohin.shohin.メーカー品番),
                    カラー = S.Min(y => y.ushohin.shohin.カラー),
                    Sku = S.Min(y => y.ushohin.uriage.Sku),
                    純売上 = S.Min(y => y.ushohin.uriage.純売上),
                    売上数量 = S.Min(y => y.ushohin.uriage.売上数量),
                    按分額 = S.Min(y => y.ushohin.uriage.按分額),
                    調整額 = S.Min(y => y.ushohin.uriage.調整額),
                    部門別シェア率 = S.Min(y => y.ushohin.uriage.部門別シェア率),
                    調整後按分額 = S.Min(y => y.ushohin.uriage.調整後按分額)
                }).ToList();

            return ListAnbunResult;
        }
        #endregion
    }
}
